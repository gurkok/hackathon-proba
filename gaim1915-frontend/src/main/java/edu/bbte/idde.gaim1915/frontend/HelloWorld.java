package edu.bbte.idde.gaim1915.frontend;

import edu.bbte.idde.gaim1915.backend.model.CarAd;
import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.DaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorld {
    private static final Logger LOG = LoggerFactory.getLogger(HelloWorld.class);

    public static void main(String[] args) {
        CarAdRepository carAdRepo = DaoFactory.getInstance().getCarAdRepository();

        LOG.info("Write out all cars {}", carAdRepo.findAll());

        CarAd carAd = carAdRepo.create(new CarAd("Volvo", 500, 20.5, "EUR", 1L));
        LOG.info("Created new car: " + carAd);

        LOG.info("Delete car with id: {}", carAd.getId());
        carAdRepo.delete(carAd.getId());
        LOG.info("Delete car with id: {}", carAd.getId());
        carAdRepo.delete(carAd.getId());

        carAd = carAdRepo.create(new CarAd("Mercedes", 5000, 2000.5, "GBP", 1L));
        LOG.info("Created new car: " + carAd);

        LOG.info("Car with id {} is: {}", carAd.getId(), carAdRepo.findByID(carAd.getId()));
        LOG.info("Car with id 0 is: {}", carAdRepo.findByID(0L));

        LOG.info("Update car {}", carAd);
        carAd.setCarName("Volvo");
        carAdRepo.update(carAd.getId(), carAd);
        LOG.info("Updated car: {}", carAd);

        carAd = carAdRepo.create(new CarAd("Volkswagen", 10000, 550.5, "RON", 1L));
        LOG.info("Created new car: " + carAd);

        LOG.info("All cars: {}", carAdRepo.findAll());
    }
}