package edu.bbte.idde.gaim1915.spring.mapper;

import edu.bbte.idde.gaim1915.spring.dto.incoming.StatisticInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.StatisticOutDto;
import edu.bbte.idde.gaim1915.spring.model.Statistic;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class StatisticMapper {

    public abstract StatisticOutDto modelToDto(Statistic statistic);

    @IterableMapping(elementTargetType = StatisticOutDto.class)
    public abstract Collection<StatisticOutDto> modelsToDtoList(Collection<Statistic> statistics);

    public abstract Statistic dtoToModel(StatisticInDto statisticInDto);

}
