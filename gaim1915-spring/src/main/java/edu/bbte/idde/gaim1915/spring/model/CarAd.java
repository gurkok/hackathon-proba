package edu.bbte.idde.gaim1915.spring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "carad")
public class CarAd extends BaseEntity {
    @Column(length = 1024, nullable = false, name = "carName")
    private String carName;
    @Column(nullable = false, name = "horsePower")
    private Integer horsePower;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private String currency;
    @Column(nullable = false)
    private Date date;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personID", nullable = false)
    private Person person;
}
