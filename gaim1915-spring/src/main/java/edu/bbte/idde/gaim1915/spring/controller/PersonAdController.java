package edu.bbte.idde.gaim1915.spring.controller;

import edu.bbte.idde.gaim1915.spring.dto.incoming.CarAdInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.CarAdOutDto;
import edu.bbte.idde.gaim1915.spring.service.PersonAdService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/api/persons/{personId}/carAds")
@Slf4j
@Profile("jpa")
public class PersonAdController {

    @Autowired
    private PersonAdService personAdService;

    @GetMapping
    public ResponseEntity<Collection<CarAdOutDto>> findCarAdsByPerson(@PathVariable("personId") Long id) {
        Collection<CarAdOutDto> carAdOutDtos = personAdService.findCarAdsByPersonId(id);
        return new ResponseEntity<>(carAdOutDtos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<CarAdOutDto> addCarAdByPerson(@PathVariable("personId") Long id,
                                                        @RequestBody @Valid CarAdInDto carAdInDto) {
        CarAdOutDto carAdOutDto = personAdService.addCarAdByPersonId(id, carAdInDto);
        return new ResponseEntity<>(carAdOutDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{carId}")
    public ResponseEntity<Collection<CarAdOutDto>> deleteCarAdByPerson(@PathVariable("personId") Long personId,
                                                                       @PathVariable("carId") Long carId) {
        personAdService.deleteCarAdByPersonId(personId, carId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
