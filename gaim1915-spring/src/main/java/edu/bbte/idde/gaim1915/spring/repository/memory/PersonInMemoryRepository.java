package edu.bbte.idde.gaim1915.spring.repository.memory;

import edu.bbte.idde.gaim1915.spring.model.Person;
import edu.bbte.idde.gaim1915.spring.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@Slf4j
@Profile("mem")
public class PersonInMemoryRepository implements PersonRepository {
    private final ConcurrentHashMap<Long, Person> personDatabase = new ConcurrentHashMap<>();
    private final AtomicLong personId = new AtomicLong(0);

    @Autowired
    private CarAdInMemoryRepository carAdRepository;

    @Override
    public Person save(Person entity) {
        if (entity.getId() == null) {
            entity.setId(personId.getAndIncrement());
            personDatabase.put(entity.getId(), entity);

            return personDatabase.get(entity.getId());
        } else {
            return update(entity.getId(), entity);
        }
    }

    @Override
    public void deleteById(Long id) {
        if (existsById(id)) {
            carAdRepository.deleteByPersonId(id);
            personDatabase.remove(id);
        } else {
            log.warn("Person with id {} does not exist! Could not delete!", id);
        }
    }

    @Override
    public Optional<Person> findById(Long id) {
        if (!existsById(id)) {
            log.warn("Person with id {} does not exist!", id);
            return Optional.empty();
        }
        return Optional.of(personDatabase.get(id));
    }

    public Person update(Long id, Person entity) {
        Person person = findById(id).orElse(null);
        if (person == null) {
            log.warn("Person with id {} does not exist!", id);
            return null;
        }
        entity.setId(id);
        entity.setCarAds(person.getCarAds());
        personDatabase.put(id, entity);
        return entity;
    }

    @Override
    public Collection<Person> findAll() {
        return personDatabase.values();
    }

    @Override
    public boolean existsById(Long id) {
        return personDatabase.containsKey(id);
    }

    @Override
    public boolean existsByUsername(String name) {
        return findByUsername(name).isPresent();
    }

    @Override
    public void deleteAllByLastLoggedInLessThan(Date date) {

    }

    @Override
    public double getAverageAge() {
        return 0;
    }

    @Override
    public Optional<Person> findByUsername(String name) {
        for (Person person : personDatabase.values()) {
            if (person.getUsername().equals(name)) {
                return Optional.of(person);
            }
        }
        return Optional.empty();
    }
}
