package edu.bbte.idde.gaim1915.spring.dto.outgoing;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class StatisticOutDto extends BaseEntityOutDto {

    Double averageAge;
    Double averagePrice;
    Date date;

}
