package edu.bbte.idde.gaim1915.spring.repository.jdbc;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.gaim1915.spring.model.Person;
import edu.bbte.idde.gaim1915.spring.repository.PersonRepository;
import edu.bbte.idde.gaim1915.spring.repository.RepoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

@Repository
@Slf4j
@Profile("jdbc")
public class PersonJdbcRepository implements PersonRepository {

    @Autowired
    private HikariDataSource hikariDataSource;

    @Override
    public Person save(Person entity) {
        if (entity.getId() == null) {
            try (Connection connection = hikariDataSource.getConnection()) {
                String query = "INSERT INTO Person (name, age, phoneNr, stars) VALUES (?, ?, ?, ?)";
                String[] idCol = {"id"};
                PreparedStatement preparedStatement = connection.prepareStatement(query, idCol);
                setPreparedStatementByPerson(entity, preparedStatement);
                int row = preparedStatement.executeUpdate();
                if (row == 0) {
                    log.warn("Did not insert anything");
                } else {
                    ResultSet resultSet = preparedStatement.getGeneratedKeys();
                    resultSet.next();
                    entity.setId(resultSet.getLong(1));
                }
                return entity;
            } catch (SQLException e) {
                log.warn("Sql exception could not create Person with id {}", entity.getId());
                throw new RepoException("Sql exception could not create Person", e);
            }
        } else {
            return update(entity.getId(), entity);
        }
    }

    @Override
    public void deleteById(Long id) {
        Optional<Person> person = findById(id);
        if (person.isPresent()) {
            try (Connection connection = hikariDataSource.getConnection()) {
                String query = "DELETE FROM Person WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(1, id);
                int row = preparedStatement.executeUpdate();
                if (row > 0) {
                    log.info("Successfully deleted person with id {}", id);
                } else {
                    log.warn("Did not delete person with id {}", id);
                }
            } catch (SQLException e) {
                log.warn("Could not delete person with id {}", id);
                throw new RepoException("Sql exception could not delete person", e);
            }
        } else {
            log.warn("The person with id {} does not exist!", id);
        }
    }

    @Override
    public Optional<Person> findById(Long id) {
        if (id == null) {
            log.warn("Id id null! Could not find!");
            return Optional.empty();
        }
        try (Connection connection = hikariDataSource.getConnection()) {
            Person person = null;
            String query = "SELECT id, name, age, phoneNr, stars FROM Person WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultset = preparedStatement.executeQuery();
            if (resultset.next()) {
                person = getPersonByResultSet(resultset);
                log.info("Found person with id {}", id);
            } else {
                log.info("Did not find person with id {}", id);
            }
            return Optional.ofNullable(person);
        } catch (SQLException e) {
            log.warn("Sql exception could not find Person with id {}", id);
            throw new RepoException("Sql exception could not put person", e);
        }
    }

    public Person update(Long id, Person entity) {
        try (Connection connection = hikariDataSource.getConnection()) {
            if (existsById(id)) {
                String query = "UPDATE Person SET name = ?, age = ?, phoneNr = ?, stars = ? WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(5, id);
                setPreparedStatementByPerson(entity, preparedStatement);
                int row = preparedStatement.executeUpdate();
                if (row == 0) {
                    log.warn("Did not update anything for id {}", id);
                }
                return findById(id).orElse(null);
            } else {
                log.warn("Person with id {} does not exist!", id);
                return null;
            }
        } catch (SQLException e) {
            log.warn("SqlException Could not update person with id {}", id);
            throw new RepoException("Sql exception could not update person", e);
        }
    }

    @Override
    public Collection<Person> findAll() {
        try (Connection connection = hikariDataSource.getConnection()) {
            Collection<Person> persons = new ArrayList<>();
            String query = "SELECT id, name, age, phoneNr, stars FROM Person";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultset = preparedStatement.executeQuery();
            while (resultset.next()) {
                Person person = getPersonByResultSet(resultset);
                persons.add(person);
            }
            return persons;
        } catch (SQLException e) {
            log.warn("Sql exception could not find persons");
            throw new RepoException("Sql exception could not find persons", e);
        }
    }

    @Override
    public boolean existsById(Long id) {
        Optional<Person> person = findById(id);
        return person.isPresent();
    }

    @Override
    public Optional<Person> findByUsername(String name) {
        if (name == null) {
            log.warn("Name is null! Could not find!");
            return Optional.empty();
        }
        try (Connection connection = hikariDataSource.getConnection()) {
            Person person = null;
            String query = "SELECT id, name, age, phoneNr, stars FROM Person WHERE name = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, name);
            ResultSet resultset = preparedStatement.executeQuery();
            if (resultset.next()) {
                person = getPersonByResultSet(resultset);
                log.info("Found person with name {}", name);
            }
            return Optional.ofNullable(person);
        } catch (SQLException e) {
            log.warn("Sql exception could not find persons");
            throw new RepoException("Sql exception could not find persons", e);
        }
    }

    @Override
    public boolean existsByUsername(String name) {
        return findByUsername(name).isPresent();
    }

    @Override
    public void deleteAllByLastLoggedInLessThan(Date date) {

    }

    @Override
    public double getAverageAge() {
        return 0;
    }

    private Person getPersonByResultSet(ResultSet resultset) throws SQLException {
        Person person = new Person();
        person.setId(resultset.getLong(1));
        person.setUsername(resultset.getString(2));
        person.setAge(resultset.getInt(3));
        person.setPhoneNr(resultset.getString(4));
        person.setStars(resultset.getDouble(5));
        return person;
    }

    private void setPreparedStatementByPerson(Person entity, PreparedStatement preparedStatement)
            throws SQLException {
        preparedStatement.setString(1, entity.getUsername());
        preparedStatement.setInt(2, entity.getAge());
        preparedStatement.setString(3, entity.getPhoneNr());
        preparedStatement.setDouble(4, entity.getStars());
    }

}
