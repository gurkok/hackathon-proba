package edu.bbte.idde.gaim1915.spring.dto.outgoing;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class CarAdOutDto extends BaseEntityOutDto {
    private String carName;
    private Integer horsePower;
    private Double price;
    private String currency;
    private PersonOutForeignDTO person;
}
