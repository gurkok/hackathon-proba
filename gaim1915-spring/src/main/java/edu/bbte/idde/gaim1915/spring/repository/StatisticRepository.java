package edu.bbte.idde.gaim1915.spring.repository;

import edu.bbte.idde.gaim1915.spring.model.Statistic;

import java.util.Date;

public interface StatisticRepository extends Repository<Statistic> {

    void deleteAllByDateLessThan(Date date);

}
