package edu.bbte.idde.gaim1915.spring.repository.jdbc;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.gaim1915.spring.model.CarAd;
import edu.bbte.idde.gaim1915.spring.model.Person;
import edu.bbte.idde.gaim1915.spring.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.spring.repository.RepoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

@Repository
@Slf4j
@Profile("jdbc")
public class CarAdJdbcRepository implements CarAdRepository {
    @Autowired
    private HikariDataSource hikariDataSource;

    @Override
    public CarAd save(CarAd entity) {
        if (entity.getId() == null) {
            try (Connection connection = hikariDataSource.getConnection()) {
                String query = "INSERT INTO CarAd (carName, horsePower, price, currency, personID)"
                        + " VALUES (?, ?, ?, ?, ?)";
                String[] idCol = {"id"};
                PreparedStatement preparedStatement = connection.prepareStatement(query, idCol);
                setPreparedStatementByCarAd(entity, preparedStatement);
                int row = preparedStatement.executeUpdate();
                if (row == 0) {
                    log.warn("Did not insert anything");
                } else {
                    ResultSet resultSet = preparedStatement.getGeneratedKeys();
                    resultSet.next();
                    entity.setId(resultSet.getLong(1));
                }
                return entity;
            } catch (SQLException e) {
                log.warn("Sql exception could not create CarAd");
                throw new RepoException("Sql exception could not create CarAd", e);
            }
        } else {
            return update(entity.getId(), entity);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (Connection connection = hikariDataSource.getConnection()) {
            Optional<CarAd> carAd = findById(id);
            if (carAd.isPresent()) {
                String query = "DELETE FROM CarAd WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(1, id);
                int row = preparedStatement.executeUpdate();
                if (row > 0) {
                    log.info("Successfully deleted car with id {}", id);
                } else {
                    log.warn("Did not delete car with id {}", id);
                }
            } else {
                log.warn("The carAd with id {} does not exist!", id);
            }
        } catch (SQLException e) {
            log.warn("Could not delete car with id {}", id);
            throw new RepoException("Could not delete car", e);
        }
    }

    @Override
    public Optional<CarAd> findById(Long id) {
        if (id == null) {
            log.warn("Id id null! Could not find!");
            return Optional.empty();
        }
        try (Connection connection = hikariDataSource.getConnection()) {
            CarAd carAd = null;
            String query = "SELECT id, carName, horsePower, price, currency, personID "
                    + "FROM CarAd WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultset = preparedStatement.executeQuery();
            if (resultset.next()) {
                carAd = getCarAdByResultSet(resultset);
                log.info("Found carAd with id {}", id);
            } else {
                log.info("Did not find carAd with id {}", id);
            }
            return Optional.ofNullable(carAd);
        } catch (SQLException e) {
            log.warn("Sql exception could not find carAd with id {}", id);
            throw new RepoException("Sql exception could not find CarAd", e);
        }
    }

    public CarAd update(Long id, CarAd entity) {
        try (Connection connection = hikariDataSource.getConnection()) {
            if (existsById(id)) {
                String query = "UPDATE CarAd SET carName = ?, horsePower = ?, price = ?, "
                        + "currency = ?, personID = ? WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                setPreparedStatementByCarAd(entity, preparedStatement);
                preparedStatement.setLong(6, id);
                int row = preparedStatement.executeUpdate();
                if (row == 0) {
                    log.warn("Did not update anything for id {}", id);
                }
                return findById(id).orElse(null);
            } else {
                log.warn("Car with id {} does not exist!", id);
                return null;
            }
        } catch (SQLException e) {
            log.warn("SqlException Could not update carAd with id {}", id);
            throw new RepoException("Sql exception could not update CarAd", e);
        }
    }

    @Override
    public Collection<CarAd> findAll() {
        try (Connection connection = hikariDataSource.getConnection()) {
            Collection<CarAd> carAds = new ArrayList<>();
            String query = "SELECT  id, carName, horsePower, price, currency, personID "
                    + "FROM CarAd";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultset = preparedStatement.executeQuery();
            while (resultset.next()) {
                CarAd carAd = getCarAdByResultSet(resultset);
                carAds.add(carAd);
            }
            return carAds;
        } catch (SQLException e) {
            log.warn("Sql exception could not find carAds");
            throw new RepoException("Sql exception could not find CarAds", e);
        }
    }

    @Override
    public boolean existsById(Long id) {
        Optional<CarAd> carAd = findById(id);
        return carAd.isPresent();
    }

    @Override
    public Collection<CarAd> findAllByCarName(String carName) {
        if (carName == null) {
            log.warn("CarName is null! Could not find!");
            return Collections.emptyList();
        }
        Collection<CarAd> carAds = new ArrayList<>();
        try (Connection connection = hikariDataSource.getConnection()) {
            String query = "SELECT  id, carName, horsePower, price, currency, personID "
                    + "FROM CarAd WHERE carName = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, carName);
            ResultSet resultset = preparedStatement.executeQuery();
            while (resultset.next()) {
                CarAd carAd = getCarAdByResultSet(resultset);
                carAds.add(carAd);
            }
            return carAds;
        } catch (SQLException e) {
            log.warn("Sql exception could not find carAds with carName {}", carName);
            throw new RepoException("Sql exception could not find CarAds", e);
        }
    }

    @Override
    public void deleteAllByDateLessThan(Date date) {

    }

    @Override
    public double getAveragePrice() {
        return 0;
    }

    private CarAd getCarAdByResultSet(ResultSet resultset) throws SQLException {
        CarAd carAd = new CarAd();
        carAd.setId(resultset.getLong(1));
        carAd.setCarName(resultset.getString(2));
        carAd.setHorsePower(resultset.getInt(3));
        carAd.setPrice(resultset.getDouble(4));
        carAd.setCurrency(resultset.getString(5));
        Person person = new Person();
        carAd.setPerson(person);
        carAd.getPerson().setId(resultset.getLong(6));
        return carAd;
    }

    private void setPreparedStatementByCarAd(CarAd entity, PreparedStatement preparedStatement)
            throws SQLException {
        preparedStatement.setString(1, entity.getCarName());
        preparedStatement.setInt(2, entity.getHorsePower());
        preparedStatement.setDouble(3, entity.getPrice());
        preparedStatement.setString(4, entity.getCurrency());
        preparedStatement.setLong(5, entity.getPerson().getId());
    }
}
