package edu.bbte.idde.gaim1915.spring.repository;

import edu.bbte.idde.gaim1915.spring.model.CarAd;

import java.util.Collection;
import java.util.Date;

public interface CarAdRepository extends Repository<CarAd> {

    Collection<CarAd> findAllByCarName(String carName);

    void deleteAllByDateLessThan(Date date);

    double getAveragePrice();
}
