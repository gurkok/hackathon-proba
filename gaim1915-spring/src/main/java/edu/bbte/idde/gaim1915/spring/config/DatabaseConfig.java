package edu.bbte.idde.gaim1915.spring.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!mem")
public class DatabaseConfig {

    @Value("${jdbc.sqlUrl}")
    private String sqlUrl;

    @Value("${jdbc.driver}")
    private String driver;

    @Value("${jdbc.username}")
    private String username;

    @Value("${jdbc.password}")
    private String password;

    @Primary
    @Bean
    public HikariDataSource getConnection() {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setUsername(username);
        hikariDataSource.setPassword(password);
        hikariDataSource.setJdbcUrl(sqlUrl);
        hikariDataSource.setDriverClassName(driver);
        return hikariDataSource;
    }

}
