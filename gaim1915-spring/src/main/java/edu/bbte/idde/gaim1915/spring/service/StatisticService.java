package edu.bbte.idde.gaim1915.spring.service;

import edu.bbte.idde.gaim1915.spring.dto.incoming.StatisticInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.StatisticOutDto;
import edu.bbte.idde.gaim1915.spring.mapper.StatisticMapper;
import edu.bbte.idde.gaim1915.spring.model.Statistic;
import edu.bbte.idde.gaim1915.spring.repository.StatisticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service
public class StatisticService {

    @Autowired
    StatisticRepository statisticRepo;

    @Autowired
    StatisticMapper statisticMapper;

    public void deleteAllByDateLessThan(Date date) {
        statisticRepo.deleteAllByDateLessThan(date);
    }

    public StatisticOutDto post(StatisticInDto statisticInDto) {
        Statistic statistic = statisticMapper.dtoToModel(statisticInDto);
        statistic.setDate(new Date());
        Statistic newStatistic = statisticRepo.save(statistic);
        return statisticMapper.modelToDto(newStatistic);
    }

    public Collection<StatisticOutDto> findAll() {
        Collection<Statistic> statistics = statisticRepo.findAll();
        return statisticMapper.modelsToDtoList(statistics);
    }

}
