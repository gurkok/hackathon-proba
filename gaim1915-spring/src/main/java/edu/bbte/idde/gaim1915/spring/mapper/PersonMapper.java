package edu.bbte.idde.gaim1915.spring.mapper;

import edu.bbte.idde.gaim1915.spring.dto.incoming.PersonInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.PersonOutDto;
import edu.bbte.idde.gaim1915.spring.model.Person;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class PersonMapper {

    public abstract PersonOutDto modelToDto(Person person);

    @IterableMapping(elementTargetType = PersonOutDto.class)
    public abstract Collection<PersonOutDto> modelsToDtoList(Collection<Person> persons);

    public abstract Person dtoToModel(PersonInDto personInDto);

}
