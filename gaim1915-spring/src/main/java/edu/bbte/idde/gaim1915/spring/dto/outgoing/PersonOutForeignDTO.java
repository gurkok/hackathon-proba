package edu.bbte.idde.gaim1915.spring.dto.outgoing;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PersonOutForeignDTO extends BaseEntityOutDto{
}
