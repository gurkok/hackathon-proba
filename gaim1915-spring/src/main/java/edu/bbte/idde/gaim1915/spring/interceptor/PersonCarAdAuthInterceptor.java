package edu.bbte.idde.gaim1915.spring.interceptor;

import edu.bbte.idde.gaim1915.spring.exception.UnauthorizedException;
import edu.bbte.idde.gaim1915.spring.service.AuthentificationService;
import edu.bbte.idde.gaim1915.spring.service.PersonAdService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class PersonCarAdAuthInterceptor implements HandlerInterceptor {

    @Autowired
    AuthentificationService authService;

    @Autowired
    PersonAdService personAdService;

    public static final String URL = "/api/persons/";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if ("GET".equals(request.getMethod())) {
            return true;
        }
        String[] params = request.getRequestURI().replace(URL, "")
                .replace("/carAds", "").split("/");
        if (params.length == 0) {
            return true;
        }
        Long personId = Long.parseLong(params[0]);
        verifyForUser(personId);
        if (params.length >= 2 && StringUtils.isNumeric(params[1])) {
            Long carId = Long.parseLong(params[1]);
            verifyForCarAd(personId, carId);
        }
        return true;
    }

    private void verifyForUser(Long personId) {
        if (!personId.equals(authService.getAuthId())) {
            log.info("Person with id {} could not modify, it is not the logged in one", personId);
            throw new UnauthorizedException("Person with id " + personId + " is not the current one logged in");
        }
    }

    private void verifyForCarAd(Long personId, Long carId) {
        log.info("Personid {} carId {}", personId, carId);
        Long personIdByCarId = personAdService.getPersonIdByCarId(carId);
        if (!personIdByCarId.equals(personId)) {
            log.info("CarAd with id {} could not be modified, it is not the logged in's one", carId);
            throw new UnauthorizedException("CarAd with id " + carId
                    + " is not the current one's who is logged in");
        }
    }

}
