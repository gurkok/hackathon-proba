package edu.bbte.idde.gaim1915.spring.repository.memory;

import edu.bbte.idde.gaim1915.spring.model.CarAd;
import edu.bbte.idde.gaim1915.spring.repository.CarAdRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@Slf4j
@Profile("mem")
public class CarAdInMemoryRepository implements CarAdRepository {
    private final ConcurrentHashMap<Long, CarAd> carAdDatabase = new ConcurrentHashMap<>();
    private final AtomicLong carAdId = new AtomicLong(0);

    @Override
    public CarAd save(CarAd carAdEntity) {
        if (carAdEntity.getId() == null) {
            carAdEntity.setId(carAdId.getAndIncrement());
            carAdDatabase.put(carAdEntity.getId(), carAdEntity);

            return carAdDatabase.get(carAdEntity.getId());
        } else {
            return update(carAdEntity.getId(), carAdEntity);
        }
    }

    @Override
    public Optional<CarAd> findById(Long carAdID) {
        if (!existsById(carAdID)) {
            log.warn("Car with id {} does not exist!", carAdID);
            return Optional.empty();
        }
        return Optional.of(carAdDatabase.get(carAdID));
    }

    @Override
    public Collection<CarAd> findAllByCarName(String carName) {
        Collection<CarAd> carAds = new ArrayList<>();
        for (CarAd carAd : carAdDatabase.values()) {
            if (carAd.getCarName().equals(carName)) {
                carAds.add(carAd);
            }
        }
        return carAds;
    }

    @Override
    public void deleteAllByDateLessThan(Date date) {

    }

    @Override
    public double getAveragePrice() {
        return 0;
    }

    public void deleteByPersonId(Long personID) {
        for (CarAd carAd : carAdDatabase.values()) {
            if (carAd.getPerson().getId().equals(personID)) {
                carAdDatabase.remove(carAd.getId());
            }
        }
    }

    @Override
    public Collection<CarAd> findAll() {
        return carAdDatabase.values();
    }

    public CarAd update(Long carID, CarAd carAd) {
        if (!existsById(carID)) {
            log.warn("Car with id {} does not exist!", carAd.getId());
            return null;
        }
        carAd.setId(carID);
        carAdDatabase.put(carID, carAd);
        return carAd;
    }

    @Override
    public void deleteById(Long carAdID) {
        if (existsById(carAdID)) {
            carAdDatabase.remove(carAdID);
        } else {
            log.warn("Car with id {} does not exist! Could not delete!", carAdID);
        }
    }

    @Override
    public boolean existsById(Long carAdID) {
        return carAdDatabase.containsKey(carAdID);
    }
}
