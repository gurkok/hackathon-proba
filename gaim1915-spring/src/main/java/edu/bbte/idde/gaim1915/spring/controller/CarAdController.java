package edu.bbte.idde.gaim1915.spring.controller;

import edu.bbte.idde.gaim1915.spring.dto.incoming.CarAdInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.CarAdOutDto;
import edu.bbte.idde.gaim1915.spring.service.CarAdService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/api/carAds")
@Slf4j
public class CarAdController {

    @Autowired
    CarAdService carAdService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<Collection<CarAdOutDto>> findAll() {
        Collection<CarAdOutDto> carAds = carAdService.findAll();
        return new ResponseEntity<>(carAds, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<CarAdOutDto> findById(@PathVariable("id") Long id) {
        CarAdOutDto carAdOutDto = carAdService.findById(id);
        return new ResponseEntity<>(carAdOutDto, HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    @ResponseBody
    public ResponseEntity<Collection<CarAdOutDto>> findByName(@PathVariable("name") String name) {
        Collection<CarAdOutDto> carAds = carAdService.findAllByCarName(name);
        return new ResponseEntity<>(carAds, HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<CarAdOutDto> post(@RequestBody @Valid CarAdInDto carAdInDto) {
        CarAdOutDto carAdOutDto = carAdService.post(carAdInDto);
        return new ResponseEntity<>(carAdOutDto, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<CarAdOutDto> put(@PathVariable("id") Long id, @RequestBody @Valid CarAdInDto carAdInDto) {
        CarAdOutDto carAdOutDto = carAdService.put(id, carAdInDto);
        return new ResponseEntity<>(carAdOutDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<CarAdOutDto> delete(@PathVariable("id") Long id) {
        carAdService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
