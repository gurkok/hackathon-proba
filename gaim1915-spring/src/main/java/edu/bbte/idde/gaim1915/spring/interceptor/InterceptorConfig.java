package edu.bbte.idde.gaim1915.spring.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@Slf4j
public class InterceptorConfig extends WebMvcConfigurerAdapter {

    @Autowired
    HttpLoggerInterceptor httpLoggerInterceptor;

    @Autowired
    CarAdAuthInterceptor carAdAuthInterceptor;

    @Autowired
    PersonAuthInterceptor personAuthInterceptor;

    @Autowired
    PersonCarAdAuthInterceptor personCarAdAuthInterceptor;

    @Override
    public void addInterceptors(final InterceptorRegistry interceptorRegistry) {
        log.info("Adding interceptors");
        interceptorRegistry.addInterceptor(httpLoggerInterceptor).addPathPatterns("/api/**");
        interceptorRegistry.addInterceptor(carAdAuthInterceptor).addPathPatterns("/api/carAds/**/");
        interceptorRegistry.addInterceptor(personCarAdAuthInterceptor).addPathPatterns("/api/persons/**/carAds/",
                "/api/persons/**/carAds/**");
        interceptorRegistry.addInterceptor(personAuthInterceptor).addPathPatterns("/api/persons/**/");
    }

}
