package edu.bbte.idde.gaim1915.spring.dto.outgoing;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public abstract class BaseEntityOutDto implements Serializable {

    Long id;

}
