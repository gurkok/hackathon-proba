package edu.bbte.idde.gaim1915.spring.repository.jpa;

import edu.bbte.idde.gaim1915.spring.model.CarAd;
import edu.bbte.idde.gaim1915.spring.repository.CarAdRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface CarAdJpaRepository extends CarAdRepository, JpaRepository<CarAd, Long> {

    @Override
    @Query("SELECT AVG(c.price) FROM CarAd c")
    double getAveragePrice();

}
