package edu.bbte.idde.gaim1915.spring.repository;

import edu.bbte.idde.gaim1915.spring.model.Person;

import java.util.Date;
import java.util.Optional;

public interface PersonRepository extends Repository<Person> {

    Optional<Person> findByUsername(String name);

    boolean existsByUsername(String name);

    void deleteAllByLastLoggedInLessThan(Date date);

    double getAverageAge();

}
