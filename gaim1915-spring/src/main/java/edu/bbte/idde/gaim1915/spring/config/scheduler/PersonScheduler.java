package edu.bbte.idde.gaim1915.spring.config.scheduler;

import edu.bbte.idde.gaim1915.spring.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Configuration
@EnableScheduling
@EnableAsync
@Slf4j
public class PersonScheduler {

    @Autowired
    PersonService personService;

    @Async
    @Transactional
    @Scheduled(cron = "0 0 * * * *")
    public void clearExpiredPersons() {
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDate = LocalDateTime.now().minusMinutes(10);
        Date date = Date.from(localDate.atZone(zoneId).toInstant());
        log.info(date.toString());
        personService.deleteAllByLastLoggedInLessThan(date);
        log.info("Carads are deleted by scheduler");
    }

}
