package edu.bbte.idde.gaim1915.spring.dto.incoming;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class CarAdInDto implements Serializable {
    @Size(max = 256)
    @NotNull
    private String carName;

    @NotNull
    private Integer horsePower;

    @NotNull
    private Double price;

    @Size(max = 256)
    @NotNull
    private String currency;

    @NotNull
    private PersonInDto person;
}
