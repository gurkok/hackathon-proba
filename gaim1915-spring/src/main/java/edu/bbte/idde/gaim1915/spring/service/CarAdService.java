package edu.bbte.idde.gaim1915.spring.service;

import edu.bbte.idde.gaim1915.spring.dto.incoming.CarAdInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.CarAdOutDto;
import edu.bbte.idde.gaim1915.spring.exception.EntityNotFoundException;
import edu.bbte.idde.gaim1915.spring.mapper.CarAdMapper;
import edu.bbte.idde.gaim1915.spring.model.CarAd;
import edu.bbte.idde.gaim1915.spring.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.spring.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service
@Slf4j
public class CarAdService implements GenericService<CarAdOutDto, CarAdInDto> {

    @Autowired
    private CarAdRepository carAdRepo;

    @Autowired
    private PersonRepository personRepo;

    @Autowired
    private CarAdMapper carAdMapper;

    @Override
    public CarAdOutDto post(CarAdInDto carAdInDto) {
        Long personId = carAdInDto.getPerson().getId();
        if (personRepo.existsById(personId)) {
            CarAd carAd = carAdMapper.dtoToModel(carAdInDto);
            carAd.setDate(new Date());
            CarAd newCarAd = carAdRepo.save(carAd);
            return carAdMapper.modelToDto(newCarAd);
        } else {
            log.error("Person with id {} does not exist!", personId);
            throw new EntityNotFoundException("Person with id " + personId + " does not exist!");
        }
    }

    @Override
    public CarAdOutDto put(Long id, CarAdInDto carAdInDto) {
        Long personId = carAdInDto.getPerson().getId();
        if (personRepo.existsById(personId)) {
            CarAd carAd = carAdRepo.findById(id).orElseThrow(() -> {
                log.error("CarAd with id {} does not exist!", id);
                throw new EntityNotFoundException("CarAd with id " + id + " does not exist!");
            });
            carAd.setPrice(carAdInDto.getPrice());
            carAd.setHorsePower(carAdInDto.getHorsePower());
            carAd.setCurrency(carAdInDto.getCurrency());
            carAd.setCarName(carAdInDto.getCarName());
            CarAd newCarAd = carAdRepo.save(carAd);
            return carAdMapper.modelToDto(newCarAd);
        } else {
            log.error("Person with id " + personId + " does not exist!");
            throw new EntityNotFoundException("Person with id " + personId + " does not exist!");
        }
    }

    @Override
    public void deleteById(Long id) {
        if (carAdRepo.existsById(id)) {
            carAdRepo.deleteById(id);
        } else {
            log.error("CarAd with id " + id + " does not exist!");
            throw new EntityNotFoundException("CarAd with id " + id + " does not exist!");
        }
    }

    @Override
    public CarAdOutDto findById(Long id) {
        if (carAdRepo.existsById(id)) {
            CarAd carAd = carAdRepo.findById(id).orElse(null);
            return carAdMapper.modelToDto(carAd);
        } else {
            log.error("CarAd with id " + id + " does not exist!");
            throw new EntityNotFoundException("CarAd with id " + id + " does not exist!");
        }
    }

    @Override
    public Collection<CarAdOutDto> findAll() {
        Collection<CarAd> carAds = carAdRepo.findAll();
        return carAdMapper.modelsToDtoList(carAds);
    }

    public Collection<CarAdOutDto> findAllByCarName(String name) {
        Collection<CarAd> carAds = carAdRepo.findAllByCarName(name);
        return carAdMapper.modelsToDtoList(carAds);
    }

    @Override
    public boolean existsById(Long id) {
        return carAdRepo.existsById(id);
    }

    public void deleteAllByDateLessThan(Date date) {
        carAdRepo.deleteAllByDateLessThan(date);
    }

    public double getAveragePrice() {
        return carAdRepo.getAveragePrice();
    }
}
