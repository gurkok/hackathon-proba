package edu.bbte.idde.gaim1915.spring.service;

import edu.bbte.idde.gaim1915.spring.dto.incoming.PersonInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.PersonOutDto;
import edu.bbte.idde.gaim1915.spring.exception.EntityAlreadyExistsException;
import edu.bbte.idde.gaim1915.spring.exception.EntityNotFoundException;
import edu.bbte.idde.gaim1915.spring.mapper.CarAdMapper;
import edu.bbte.idde.gaim1915.spring.mapper.PersonMapper;
import edu.bbte.idde.gaim1915.spring.model.Person;
import edu.bbte.idde.gaim1915.spring.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.spring.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;

@Service
@Slf4j
public class PersonService implements GenericService<PersonOutDto, PersonInDto> {

    @Autowired
    PersonRepository personRepo;

    @Autowired
    PersonMapper personMapper;

    @Autowired
    CarAdRepository carAdRepo;

    @Autowired
    CarAdMapper carAdMapper;

    @Caching(evict = {
            @CacheEvict(value = "persons", allEntries = true),
            @CacheEvict(value = "personName", allEntries = true),
            @CacheEvict(value = "person", key = "#id")
    })
    @Override
    public void deleteById(Long id) {
        if (personRepo.existsById(id)) {
            log.info("Deletes person with id {}", id);
            personRepo.deleteById(id);
        } else {
            log.error("Person with id {} does not exist!", id);
            throw new EntityNotFoundException("Person with id " + id + " does not exist!");
        }
    }

    @Cacheable("person")
    @Override
    public PersonOutDto findById(Long id) {
        if (personRepo.existsById(id)) {
            Person person = personRepo.findById(id).orElse(null);
            return personMapper.modelToDto(person);
        } else {
            log.error("Person with id " + id + " does not exist!");
            throw new EntityNotFoundException("Person with id " + id + " does not exist!");
        }
    }

    @Cacheable("persons")
    @Override
    public Collection<PersonOutDto> findAll() {
        Collection<Person> persons = personRepo.findAll();
        return personMapper.modelsToDtoList(persons);
    }

    @Cacheable("personName")
    public PersonOutDto findByName(String name) {
        Person person = personRepo.findByUsername(name).orElse(null);
        if (person == null) {
            throw new EntityNotFoundException("Person with username " + name + " does not exist");
        }
        return personMapper.modelToDto(person);
    }

    @Override
    public boolean existsById(Long id) {
        return personRepo.existsById(id);
    }

    public boolean existsByName(String name) {
        return personRepo.existsByUsername(name);
    }

    @Caching(evict = @CacheEvict(value = "persons", allEntries = true))
    @Override
    public PersonOutDto post(PersonInDto personInDto) {
        String username = personInDto.getUsername();
        if (existsByName(username)) {
            log.error("The name {} already exists!!", username);
            throw new EntityAlreadyExistsException("The name " + username + " already exists!");
        }
        Person person = personMapper.dtoToModel(personInDto);
        person.setPassword(new BCryptPasswordEncoder().encode(person.getPassword()));
        person.setLastLoggedIn(new Date());
        Person newPerson = personRepo.save(person);
        return personMapper.modelToDto(newPerson);
    }

    @Caching(evict = {
            @CacheEvict(value = "persons", allEntries = true),
            @CacheEvict(value = "person", key = "#id"),
            @CacheEvict(value = "personName", key = "#personInDto.username")
    })
    @Transactional
    @Override
    public PersonOutDto put(Long id, PersonInDto personInDto) {
        Person person = personRepo.findById(id).orElseThrow(() -> {
            log.error("Person with id " + id + " does not exist!");
            throw new EntityNotFoundException("Person with id " + id + " does not exist!");
        });
        person.setPhoneNr(personInDto.getPhoneNr());
        person.setAge(personInDto.getAge());
        person.setStars(personInDto.getStars());
        Person newPerson = personRepo.save(person);
        return personMapper.modelToDto(newPerson);
    }

    @Caching(evict = {
            @CacheEvict(value = "persons", allEntries = true),
            @CacheEvict(value = "person", allEntries = true),
            @CacheEvict(value = "personName", allEntries = true)
    })
    public void deleteAllByLastLoggedInLessThan(Date lastLoggedIn) {
        personRepo.deleteAllByLastLoggedInLessThan(lastLoggedIn);
    }

    public double getAverageAge() {
        return personRepo.getAverageAge();
    }

}
