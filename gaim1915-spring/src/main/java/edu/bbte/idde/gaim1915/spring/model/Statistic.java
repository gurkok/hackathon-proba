package edu.bbte.idde.gaim1915.spring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Statistic extends BaseEntity {
    @Column(nullable = false)
    private Double averageAge;
    @Column(nullable = false)
    private Double averagePrice;
    @Column(nullable = false)
    private Date date;
}
