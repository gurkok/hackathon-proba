package edu.bbte.idde.gaim1915.spring.service;

import java.util.Collection;

public interface GenericService<T, K> {

    void deleteById(Long id);

    T findById(Long id);

    Collection<T> findAll();

    boolean existsById(Long id);

    T post(K entity);

    T put(Long id, K entity);

}
