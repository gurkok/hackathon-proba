package edu.bbte.idde.gaim1915.spring.repository;

import edu.bbte.idde.gaim1915.spring.model.BaseEntity;

import java.util.Collection;
import java.util.Optional;

public interface Repository<T extends BaseEntity> {

    T save(T entity);

    void deleteById(Long id);

    Optional<T> findById(Long id);

    Collection<T> findAll();

    boolean existsById(Long id);

}
