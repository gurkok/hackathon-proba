package edu.bbte.idde.gaim1915.spring.interceptor;

import edu.bbte.idde.gaim1915.spring.exception.UnauthorizedException;
import edu.bbte.idde.gaim1915.spring.service.AuthentificationService;
import edu.bbte.idde.gaim1915.spring.service.PersonAdService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class CarAdAuthInterceptor implements HandlerInterceptor {

    @Autowired
    AuthentificationService authService;

    @Autowired
    PersonAdService personAdService;

    public static final String URL = "/api/carAds/";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if ("GET".equals(request.getMethod())) {
            return true;
        }
        String param = request.getRequestURI().replace(URL, "");
        if (StringUtils.isNumeric(param)) {
            Long carId = (long) Integer.parseInt(param);
            Long personId = authService.getAuthId();
            Long personIdByCarId = personAdService.getPersonIdByCarId(carId);
            log.info("{} person's car {} logged in: {}", personIdByCarId, carId, personId);
            if (!personIdByCarId.equals(personId)) {
                log.info("CarAd with id {} could not be modified, it is not the logged in's one", carId);
                throw new UnauthorizedException("CarAd with id " + carId
                        + " is not the current one's who is logged in");
            }
        }
        return true;
    }

}
