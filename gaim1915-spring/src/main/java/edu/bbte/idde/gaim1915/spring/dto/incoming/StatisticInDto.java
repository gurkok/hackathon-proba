package edu.bbte.idde.gaim1915.spring.dto.incoming;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class StatisticInDto {

    @NotNull
    Double averageAge;
    @NotNull
    Double averagePrice;
    @NotNull
    Date date;

}
