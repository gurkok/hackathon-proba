package edu.bbte.idde.gaim1915.spring.repository.jpa;

import edu.bbte.idde.gaim1915.spring.model.Statistic;
import edu.bbte.idde.gaim1915.spring.repository.StatisticRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface StatisticJpaRepository extends StatisticRepository, JpaRepository<Statistic, Long> {

}
