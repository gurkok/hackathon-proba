package edu.bbte.idde.gaim1915.spring.service;

import edu.bbte.idde.gaim1915.spring.dto.incoming.CarAdInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.CarAdOutDto;
import edu.bbte.idde.gaim1915.spring.exception.EntityNotFoundException;
import edu.bbte.idde.gaim1915.spring.mapper.CarAdMapper;
import edu.bbte.idde.gaim1915.spring.mapper.PersonMapper;
import edu.bbte.idde.gaim1915.spring.model.CarAd;
import edu.bbte.idde.gaim1915.spring.model.Person;
import edu.bbte.idde.gaim1915.spring.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.spring.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class PersonAdService {

    @Autowired
    PersonRepository personRepo;

    @Autowired
    PersonMapper personMapper;

    @Autowired
    CarAdRepository carAdRepo;

    @Autowired
    CarAdMapper carAdMapper;

    public Collection<CarAdOutDto> findCarAdsByPersonId(Long id) {
        Person person = personRepo.findById(id).orElseThrow(() -> {
            log.error("Person with id {} does not exist!", id);
            throw new EntityNotFoundException("Person with id " + id + " does not exist!");
        });
        return carAdMapper.modelsToDtoList(person.getCarAds());
    }

    public CarAdOutDto addCarAdByPersonId(Long id, CarAdInDto carAdInDto) {
        Person person = personRepo.findById(id).orElseThrow(() -> {
            log.error("Person with id " + id + " does not exist!");
            throw new EntityNotFoundException("Person with id " + id + " does not exist!");
        });
        CarAd carAd = carAdMapper.dtoToModel(carAdInDto);
        carAd.getPerson().setId(id);
        carAd.setDate(new Date());
        person.getCarAds().add(carAd);
        personRepo.save(person);
        List<CarAd> carAds = person.getCarAds();
        return carAdMapper.modelToDto(carAds.get(carAds.size() - 1));
    }

    public void deleteCarAdByPersonId(Long personId, Long carId) {
        CarAd carAd = carAdRepo.findById(carId).orElseThrow(() -> {
            log.error("CarAd with id {} does not exist!", carId);
            throw new EntityNotFoundException("CarAd with id " + carId + " does not exist!");
        });
        Person person = personRepo.findById(personId).orElseThrow(() -> {
            log.error("Person with id " + personId + " does not exist!");
            throw new EntityNotFoundException("Person with id " + personId + " does not exist!");
        });
        person.getCarAds().remove(carAd);
        personRepo.save(person);
    }

    public Long getPersonIdByCarId(Long carId) {
        CarAd carAd = carAdRepo.findById(carId).orElseThrow(() -> {
            log.error("CarAd with id " + carId + " does not exist!");
            throw new EntityNotFoundException("CarAd with id " + carId + " does not exist!");
        });
        return carAd.getPerson().getId();
    }

}
