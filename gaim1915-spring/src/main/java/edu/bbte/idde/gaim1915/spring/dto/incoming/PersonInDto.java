package edu.bbte.idde.gaim1915.spring.dto.incoming;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class PersonInDto implements Serializable {
    private Long id;

    @Size(max = 256)
    @NotNull
    private String username;

    @Size(max = 256)
    @NotNull
    private String password;

    @NotNull
    private Integer age;

    @Size(max = 256)
    @NotNull
    private String phoneNr;

    @NotNull
    private Double stars;
}
