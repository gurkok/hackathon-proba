package edu.bbte.idde.gaim1915.spring.service.utils;

public class ServiceUtils {

    public static final Long MAX_LONG = 10000000000000000L;

    public static final Integer STRING_MAX_SIZE = 256;

}
