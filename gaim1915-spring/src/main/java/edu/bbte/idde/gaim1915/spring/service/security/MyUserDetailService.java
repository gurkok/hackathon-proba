package edu.bbte.idde.gaim1915.spring.service.security;

import edu.bbte.idde.gaim1915.spring.dto.security.SecurityUser;
import edu.bbte.idde.gaim1915.spring.model.Person;
import edu.bbte.idde.gaim1915.spring.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private PersonRepository personRepo;

    @Override
    public UserDetails loadUserByUsername(String username) {
        Person person = personRepo.findByUsername(username).orElse(null);
        return new SecurityUser(person);
    }

}
