package edu.bbte.idde.gaim1915.spring.dto.outgoing;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class PersonOutDto extends BaseEntityOutDto {
    private String username;
    private Integer age;
    private String phoneNr;
    private Double stars;
//    private String password;
}
