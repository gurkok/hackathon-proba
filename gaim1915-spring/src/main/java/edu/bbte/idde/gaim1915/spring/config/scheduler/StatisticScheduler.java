package edu.bbte.idde.gaim1915.spring.config.scheduler;

import edu.bbte.idde.gaim1915.spring.dto.incoming.StatisticInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.StatisticOutDto;
import edu.bbte.idde.gaim1915.spring.service.CarAdService;
import edu.bbte.idde.gaim1915.spring.service.PersonService;
import edu.bbte.idde.gaim1915.spring.service.StatisticService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Configuration
@EnableScheduling
@EnableAsync
@Slf4j
public class StatisticScheduler {

    @Autowired
    CarAdService carAdService;

    @Autowired
    PersonService personService;

    @Autowired
    StatisticService statisticService;

    @Async
    @Transactional
    @Scheduled
    public void clearExpiredStatistics() {
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDate = LocalDateTime.now().minusMinutes(10);
        Date date = Date.from(localDate.atZone(zoneId).toInstant());
        log.info(date.toString());
        statisticService.deleteAllByDateLessThan(date);
        log.info("Statistics are deleted by scheduler");
    }

    @Async
    @Transactional
    @Scheduled(cron = "0 * * * * *")
    public void generateStatistics() {
        log.info("Generating statistics");
        double price = carAdService.getAveragePrice();
        double age = personService.getAverageAge();
        StatisticInDto statisticInDto = new StatisticInDto();
        statisticInDto.setAveragePrice(price);
        statisticInDto.setAverageAge(age);
        statisticInDto.setDate(new Date());
        StatisticOutDto statisticOutDto = statisticService.post(statisticInDto);
        log.info("The statistics is: {}", statisticOutDto.toString());
    }

}
