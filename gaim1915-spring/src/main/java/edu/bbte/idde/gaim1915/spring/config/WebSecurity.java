package edu.bbte.idde.gaim1915.spring.config;

import com.google.gson.Gson;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.AuthentificationDto;
import edu.bbte.idde.gaim1915.spring.dto.security.SecurityUser;
import edu.bbte.idde.gaim1915.spring.exception.EntityNotFoundException;
import edu.bbte.idde.gaim1915.spring.model.Person;
import edu.bbte.idde.gaim1915.spring.repository.PersonRepository;
import edu.bbte.idde.gaim1915.spring.service.security.MyUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.Executor;

@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    PersonRepository personRepo;

    @Override
    @Bean
    public UserDetailsService userDetailsServiceBean() {
        return new MyUserDetailService();
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceBean()).passwordEncoder(passwordEncoder());
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST",
                "PUT", "DELETE", "PATCH", "OPTIONS", "HEAD", "TRACE"));
        configuration.setExposedHeaders(Collections.singletonList("*"));
        configuration.setAllowedHeaders(Collections.singletonList("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and()
                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
                .and()
                .headers().httpStrictTransportSecurity().disable()
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .successHandler(successHandler())
                .failureHandler(failureHandler())
                .and()
                .logout()
                .logoutUrl("/logout")
                .deleteCookies("JSESSIONID")
                .logoutSuccessHandler(logoutSuccessHandler())
                .and()
                .authorizeRequests()
                .mvcMatchers(HttpMethod.DELETE, "/api/persons").hasRole("ADMIN")
                .mvcMatchers(HttpMethod.POST, "/api/persons").anonymous()
                .mvcMatchers(HttpMethod.GET, "/api/carAds").authenticated()
                .mvcMatchers(HttpMethod.POST, "/api/carAds").authenticated()
                .mvcMatchers(HttpMethod.DELETE, "/api/carAds").authenticated()
                .mvcMatchers(HttpMethod.PUT, "/api/carAds").authenticated()
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    private AuthenticationSuccessHandler successHandler() {
        return (httpServletRequest, httpServletResponse, authentication) -> {
            httpServletResponse.setContentType("application/json");
            httpServletResponse.setCharacterEncoding("UTF-8");

            SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
            AuthentificationDto authDto = new AuthentificationDto();
            authDto.setUsername(authentication.getName());
            authDto.setRoles(authentication.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority).distinct().toArray(String[]::new));
            authDto.setId(securityUser.getPerson().getId());
            setLastLoggedIn(securityUser.getPerson().getId());
            httpServletResponse.getWriter().write(new Gson().toJson(authDto));
            httpServletResponse.setStatus(200);
        };
    }

    private AuthenticationFailureHandler failureHandler() {
        return (httpServletRequest, httpServletResponse, e) -> {
            httpServletResponse.setContentType("application/json");
            httpServletResponse.setCharacterEncoding("UTF-8");
            httpServletResponse.getWriter().write(new Gson().toJson("{error: Couldn't authenticate!}"));
            httpServletResponse.setStatus(401);
        };
    }

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return (httpServletRequest, httpServletResponse, authentication) -> {
            httpServletResponse.setContentType("application/json");
            httpServletResponse.setCharacterEncoding("UTF-8");
            httpServletResponse.setStatus(200);
        };
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Executor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }

    private void setLastLoggedIn(Long id) {
        Person person = personRepo.findById(id).orElse(null);
        if (person == null) {
            throw new EntityNotFoundException("The person with id " + id + " does not exist!");
        }
        person.setLastLoggedIn(new Date());
        personRepo.save(person);
    }

}
