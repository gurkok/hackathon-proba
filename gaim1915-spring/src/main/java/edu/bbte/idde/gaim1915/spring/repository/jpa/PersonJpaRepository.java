package edu.bbte.idde.gaim1915.spring.repository.jpa;

import edu.bbte.idde.gaim1915.spring.model.Person;
import edu.bbte.idde.gaim1915.spring.repository.PersonRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface PersonJpaRepository extends PersonRepository, JpaRepository<Person, Long> {

    @Override
    @Query("SELECT AVG(p.age) FROM Person p")
    double getAverageAge();

}
