package edu.bbte.idde.gaim1915.spring.mapper;

import edu.bbte.idde.gaim1915.spring.dto.incoming.CarAdInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.CarAdOutDto;
import edu.bbte.idde.gaim1915.spring.model.CarAd;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class CarAdMapper {

    public abstract CarAdOutDto modelToDto(CarAd carAd);

    @IterableMapping(elementTargetType = CarAdOutDto.class)
    public abstract Collection<CarAdOutDto> modelsToDtoList(Collection<CarAd> carAds);
    
    public abstract CarAd dtoToModel(CarAdInDto carAdInDto);

}
