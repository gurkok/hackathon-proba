package edu.bbte.idde.gaim1915.spring.controller;

import edu.bbte.idde.gaim1915.spring.dto.outgoing.StatisticOutDto;
import edu.bbte.idde.gaim1915.spring.service.StatisticService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/statistics")
@Slf4j
public class StatisticController {

    @Autowired
    StatisticService statisticService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<Collection<StatisticOutDto>> findAll() {
        Collection<StatisticOutDto> statistics = statisticService.findAll();
        return new ResponseEntity<>(statistics, HttpStatus.OK);
    }

}
