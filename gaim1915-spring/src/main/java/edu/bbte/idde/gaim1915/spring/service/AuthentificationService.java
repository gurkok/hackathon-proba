package edu.bbte.idde.gaim1915.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthentificationService {

    @Autowired
    PersonService personService;

    @Autowired
    PersonAdService personAdService;

    public String getAuthUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    public Long getAuthId() {
        String name = getAuthUsername();
        return personService.findByName(name).getId();
    }

}
