package edu.bbte.idde.gaim1915.spring.controller;

import edu.bbte.idde.gaim1915.spring.dto.incoming.PersonInDto;
import edu.bbte.idde.gaim1915.spring.dto.outgoing.PersonOutDto;
import edu.bbte.idde.gaim1915.spring.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/api/persons")
@Slf4j
public class PersonController {

    @Autowired
    PersonService personService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<Collection<PersonOutDto>> findAll() {
        Collection<PersonOutDto> persons = personService.findAll();
        return new ResponseEntity<>(persons, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ResponseEntity<PersonOutDto> findById(@PathVariable("id") Long id) {
        PersonOutDto person = personService.findById(id);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    @ResponseBody
    public ResponseEntity<PersonOutDto> findByName(@PathVariable("name") String name) {
        PersonOutDto person = personService.findByName(name);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<PersonOutDto> post(@RequestBody @Valid PersonInDto personInDto) {
        PersonOutDto person = personService.post(personInDto);
        return new ResponseEntity<>(person, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<PersonOutDto> put(@PathVariable("id") Long id, @RequestBody @Valid PersonInDto personInDto) {
        PersonOutDto person = personService.put(id, personInDto);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<PersonOutDto> delete(@PathVariable("id") Long id) {
        personService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
