package edu.bbte.idde.gaim1915.spring.interceptor;

import edu.bbte.idde.gaim1915.spring.exception.UnauthorizedException;
import edu.bbte.idde.gaim1915.spring.service.AuthentificationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class PersonAuthInterceptor implements HandlerInterceptor {

    @Autowired
    AuthentificationService authService;

    public static final String URL = "/api/persons/";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if ("GET".equals(request.getMethod())) {
            return true;
        }
        String param = request.getRequestURI().replace(URL, "");
        if (StringUtils.isNumeric(param)) {
            Long id = Long.parseLong(param);
            log.info("person with id: {}", id);
            if (!id.equals(authService.getAuthId())) {
                log.info("Person with id {} could not modify, it is not the logged in one", id);
                throw new UnauthorizedException("Person with id " + id + " is not the current one logged in");
            }
        }
        return true;
    }

}
