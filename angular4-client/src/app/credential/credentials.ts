export class Credentials {

    username: string;
    role: string[];
    id: number;

    constructor(){
        this.username = '';
        this.role = [];
        this.id = 0;
    }
  
  }