import { Injectable } from '@angular/core';
import { Credentials } from './credentials';

@Injectable({
  providedIn: 'root'
})
export class CredentialService {

  constructor() { }

  public getCredentials() {
    let credentials = new Credentials();
    let jsonCredentials = localStorage.getItem('credentials');
    if (jsonCredentials != null) {
      jsonCredentials = JSON.parse(jsonCredentials);
      if (jsonCredentials != null) {
        credentials.username = jsonCredentials["username"];
        credentials.role = jsonCredentials["role"];
        credentials.id = jsonCredentials["id"];
      }
    }
    return credentials;
  }
}
