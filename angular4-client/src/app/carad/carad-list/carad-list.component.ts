import { Component, OnInit } from '@angular/core';
import { Carad } from '../carad';
import { CaradService } from '../carad.service';

@Component({
  selector: 'app-carad-list',
  templateUrl: './carad-list.component.html',
  styleUrls: ['./carad-list.component.css']
})
export class CaradListComponent implements OnInit {

  carads: Carad[];
  errorMsg: string;
  showAddCarad: boolean;
  carName: string;

  constructor(private caradService: CaradService) {
    this.carads = [];
    this.errorMsg = '';
    this.showAddCarad = false;
    this.carName = '';
  }

  ngOnInit() {
    this.caradService.findAll().subscribe(data => {
        this.carads = data;
      },
      () => {
        this.errorMsg = 'Could not find carads!';
      })
  }

  public onDelete(carad: Carad) {
    const observer = {
      next: () => {
        this.carads = this.carads.filter(deletedCarad => deletedCarad.id !== carad.id);
      },
      error: () => this.errorMsg = 'Could not delete request!',
      complete: () => { },
    };
    this.caradService.delete(carad.id).subscribe(observer);
  }

  public onShowAddCarad() {
    this.showAddCarad = true;
  }

  public onHideAddCarad() {
    this.showAddCarad = false;
  }

  public addCarad(carad: Carad) {
    this.carads.push(carad);
    this.showAddCarad = false;
  }

}
