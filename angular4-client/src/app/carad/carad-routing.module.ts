import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaradByuserComponent } from './carad-byuser/carad-byuser.component';
import { CaradProfileComponent } from './carad-profile/carad-profile.component';

const routes: Routes = [
  { path: "api/persons/:id/carAds", component: CaradByuserComponent },
  { path: "api/carAds", component: CaradByuserComponent },
  { path: "api/carAds/:id", component: CaradProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaradRoutingModule { }
