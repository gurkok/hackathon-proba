import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaradRoutingModule } from './carad-routing.module';
import { CaradListComponent } from './carad-list/carad-list.component';
import { CaradPipe } from './carad.pipe';
import { CaradFormComponent } from './carad-form/carad-form.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CaradByuserComponent } from './carad-byuser/carad-byuser.component';
import { CaradProfileComponent } from './carad-profile/carad-profile.component';


@NgModule({
  declarations: [
    CaradListComponent,
    CaradPipe,
    CaradFormComponent,
    CaradByuserComponent,
    CaradProfileComponent
  ],
  imports: [
    CommonModule,
    CaradRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class CaradModule { }
