import { Person } from "../person/person";

export class Carad {

    id: number;
    carName: string;
    horsePower: number;
    price: number;
    currency: string;
    person: Person;

    constructor() {
        this.id = 0;
        this.carName = '';
        this.horsePower = 0;
        this.price = 0;
        this.currency = '';
        this.person = new Person();
    }

}
