import { TestBed } from '@angular/core/testing';

import { CaradService } from './carad.service';

describe('CaradService', () => {
  let service: CaradService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaradService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
