import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaradByuserComponent } from './carad-byuser.component';

describe('CaradByuserComponent', () => {
  let component: CaradByuserComponent;
  let fixture: ComponentFixture<CaradByuserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaradByuserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaradByuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
