import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CredentialService } from 'src/app/credential/credential.service';
import { Credentials } from 'src/app/credential/credentials';
import { Carad } from '../carad';
import { CaradService } from '../carad.service';

@Component({
  selector: 'app-carad-byuser',
  templateUrl: './carad-byuser.component.html',
  styleUrls: ['./carad-byuser.component.css']
})
export class CaradByuserComponent implements OnInit {

  carads: Carad[];
  errorMsg: string;
  showAddCarad: boolean;
  carName: string;
  id: any;
  credentials: Credentials;

  constructor(private caradService: CaradService, private route: ActivatedRoute, credentialService: CredentialService) {
    this.carads = [];
    this.errorMsg = '';
    this.showAddCarad = false;
    this.carName = '';
    this.id = this.route.snapshot.paramMap.get('id');
    this.credentials = credentialService.getCredentials();
  }

  ngOnInit() {
    if (this.id == null) {
      this.caradService.findAll().subscribe(data => {
        this.carads = data;
      },
        () => {
          this.errorMsg = 'Could not find carads!';
        })
    } else {
      this.caradService.findAllByUser(this.id).subscribe(data => {
        this.carads = data;
      },
        () => {
          this.errorMsg = 'Could not find carads!';
        })
    }
  }

  public onDelete(carad: Carad) {
    const observer = {
      next: () => {
        this.carads = this.carads.filter(deletedCarad => deletedCarad.id !== carad.id);
      },
      error: () => this.errorMsg = 'Could not delete request!',
      complete: () => { },
    };
    this.caradService.delete(carad.id).subscribe(observer);
  }

  public onShowAddCarad() {
    this.showAddCarad = true;
  }

  public onHideAddCarad() {
    this.showAddCarad = false;
  }

  public addCarad(carad: Carad) {
    this.carads.push(carad);
    this.showAddCarad = false;
  }

}
