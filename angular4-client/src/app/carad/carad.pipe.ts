import { Pipe, PipeTransform } from '@angular/core';
import { Carad } from './carad';

@Pipe({
  name: 'caradFilter'
})
export class CaradPipe implements PipeTransform {

  transform(value: Carad[], input: string): any {
    if (input) {
      return value.filter(val => val.carName.indexOf(input) >= 0);
    } else {
      return value;
    }
  }

}
