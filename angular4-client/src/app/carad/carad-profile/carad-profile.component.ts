import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CredentialService } from 'src/app/credential/credential.service';
import { Credentials } from 'src/app/credential/credentials';
import { Carad } from '../carad';
import { CaradService } from '../carad.service';

@Component({
  selector: 'app-carad-profile',
  templateUrl: './carad-profile.component.html',
  styleUrls: ['./carad-profile.component.css']
})
export class CaradProfileComponent implements OnInit {

  carad: Carad;
  id: number;
  credentials: Credentials;
  isDataLoaded: boolean;
  onEdit: boolean;
  errorMessage: string;

  constructor(private caradService: CaradService, credentialService: CredentialService,
    routeParams: ActivatedRoute,
    private router: Router) {
    this.credentials = credentialService.getCredentials();
    this.isDataLoaded = false;
    this.onEdit = true;
    this.id = routeParams.snapshot.params['id'];
    this.errorMessage = '';
    this.carad = new Carad();
  }

  ngOnInit() {
    this.caradService.findCarad(this.id).subscribe(data => {
      this.carad = data;
      this.isDataLoaded = true;
    },
      () => {
        this.errorMessage = 'Could not find carad!';
      });
  }

  onDelete(carad: Carad) {
    this.caradService.delete(carad.id).subscribe(() => {
      if (carad.person.id == this.credentials.id) {
        localStorage.clear();
        this.router.navigate(['/api/persons/' + this.credentials.id + '/carAds']);
      } else {
        this.router.navigate(['/api/carAds']);
      }
    },
      () => {
        this.errorMessage = 'Could not delete carad!';
      });
  }

  onUpdate() {
    this.caradService.findCarad(this.id).subscribe(() => {
      this.errorMessage = '';
      this.caradService.update(this.carad).subscribe();
    },
      () => {
        this.errorMessage = 'Could not update carad!';
      });
  }

  onEditClick() {
    this.onEdit = !this.onEdit;
  }

}
