import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaradProfileComponent } from './carad-profile.component';

describe('CaradProfileComponent', () => {
  let component: CaradProfileComponent;
  let fixture: ComponentFixture<CaradProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaradProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaradProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
