import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Carad } from './carad';

@Injectable({
  providedIn: 'root'
})
export class CaradService {

  private url: string;
  private personUrl: string;

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080/api/carAds';
    this.personUrl = 'http://localhost:8080/api/persons';
  }

  public findAll(): Observable<Carad[]> {
    return this.http.get<Carad[]>(this.url, { withCredentials: true });
  }

  public findAllByUser(id: number): Observable<Carad[]> {
    return this.http.get<Carad[]>(this.personUrl + "/" + id + "/carAds", { withCredentials: true });
  }

  public findCarad(id: number): Observable<Carad> {
    console.log(this.url);
    return this.http.get<Carad>(this.url + "/" + id, { withCredentials: true });
  }

  public delete(id: number) {
    return this.http.delete(this.url + '/' + id, { withCredentials: true });
  }

  public save(carad: Carad) {
    return this.http.post<Carad>(this.url, carad, { withCredentials: true });
  }

  public update(carad: Carad) {
    return this.http.put<Carad>(this.url + "/" + carad.id, carad, { withCredentials: true });
  }
}
