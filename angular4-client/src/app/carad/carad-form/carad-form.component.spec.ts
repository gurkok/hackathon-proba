import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaradFormComponent } from './carad-form.component';

describe('CaradFormComponent', () => {
  let component: CaradFormComponent;
  let fixture: ComponentFixture<CaradFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaradFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaradFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
