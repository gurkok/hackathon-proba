import { Component, OnInit, Output } from '@angular/core';
import { CredentialService } from 'src/app/credential/credential.service';
import { Credentials } from 'src/app/credential/credentials';
import { EventEmitter } from  '@angular/core';
import { Carad } from '../carad';
import { CaradService } from '../carad.service';

@Component({
  selector: 'app-carad-form',
  templateUrl: './carad-form.component.html',
  styleUrls: ['./carad-form.component.css']
})
export class CaradFormComponent implements OnInit {

  carad: Carad
  credentials: Credentials
  errorMsg: string;
  @Output() newCaradEvent = new EventEmitter<Carad>();

  constructor(private caradService: CaradService, credentialService: CredentialService) {
    this.carad = new Carad();
    this.credentials = credentialService.getCredentials();
    this.carad.person.id = this.credentials.id;
    this.errorMsg = '';
  }

  ngOnInit() {
    ;
  }

  public onAdd() {
    this.caradService.save(this.carad).subscribe(
      (data) => {
        this.newCaradEvent.emit(data);
      },
      () => {
        this.errorMsg = 'Could not add carad!'
      }
    )
  }

}
