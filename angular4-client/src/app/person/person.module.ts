import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonRoutingModule } from './person-routing.module';
import { PersonListComponent } from './person-list/person-list.component';
import { MenuComponent } from '../menu/menu/menu.component';
import { FormsModule } from '@angular/forms';
import { PersonPipe } from './person.pipe';
import { PersonRegistrationComponent } from './person-registration/person-registration.component';
import { PersonLoginComponent } from './person-login/person-login.component';
import { PersonProfileComponent } from './person-profile/person-profile.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    PersonListComponent,
    PersonPipe,
    PersonRegistrationComponent,
    PersonLoginComponent,
    PersonProfileComponent
  ],
  imports: [
    CommonModule,
    PersonRoutingModule,
    FormsModule,
    SharedModule
  ],
})
export class PersonModule { }
