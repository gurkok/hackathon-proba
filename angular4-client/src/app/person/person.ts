export class Person {

    id: number;
    username: string;
    age: number;
    phoneNr: string;
    stars: number;
    password: string;

    constructor() {
        this.id = 0;
        this.username = '';
        this.age = 0;
        this.phoneNr = '';
        this.stars = 0;
        this.password = '';
    }

}
