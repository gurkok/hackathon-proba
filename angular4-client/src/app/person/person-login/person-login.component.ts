import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { PersonService } from '../person.service';
import { Router } from "@angular/router";
import { Credentials } from 'src/app/credential/credentials';
import { CredentialService } from 'src/app/credential/credential.service';

@Component({
  selector: 'app-person-login',
  templateUrl: './person-login.component.html',
  styleUrls: ['./person-login.component.css']
})
export class PersonLoginComponent implements OnInit {

  user: Person;
  errorMsg: string;
  // header: string;
  credentials: Credentials

  constructor(
    private router: Router,
    private personService: PersonService,
    private credentialService: CredentialService) {
    this.user = new Person();
    this.errorMsg = '';
    this.credentials = credentialService.getCredentials();
    // this.header = 'Basic ' + window.btoa(this.credentials.username + ':' + this.credentials.pa);
  }

  ngOnInit() { 
    ;
  }

  public onLogin() {
    this.personService.login(this.user).subscribe((resp) => {
      let credentials = new Credentials();
      credentials.username = resp["username"];
      credentials.role = resp["roles"];
      credentials.id = resp["id"];
      localStorage.setItem('credentials', JSON.stringify(credentials));
      this.router.navigate(['api/persons']);
    },
      (err) => {
        this.errorMsg = 'The password or username is not correct!';
      });
  }

}
