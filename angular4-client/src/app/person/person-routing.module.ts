import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonListComponent} from './person-list/person-list.component';
import { PersonLoginComponent } from './person-login/person-login.component';
import { PersonProfileComponent } from './person-profile/person-profile.component';
import { PersonRegistrationComponent } from './person-registration/person-registration.component';

const routes: Routes = [
  {path: "api/persons", component: PersonListComponent},
  {path: "api/login", component: PersonLoginComponent},
  {path: "api/registration", component: PersonRegistrationComponent},
  {path: "api/persons/:id", component: PersonProfileComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonRoutingModule { }
