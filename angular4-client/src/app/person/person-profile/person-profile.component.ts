import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CredentialService } from 'src/app/credential/credential.service';
import { Credentials } from 'src/app/credential/credentials';
import { Person } from '../person';
import { PersonService } from '../person.service';
import * as bcrypt from 'bcryptjs'

@Component({
  selector: 'app-person-profile',
  templateUrl: './person-profile.component.html',
  styleUrls: ['./person-profile.component.css']
})
export class PersonProfileComponent implements OnInit {

  user: Person;
  id: number;
  credentials: Credentials;
  isDataLoaded: boolean;
  onEdit: boolean;
  errorMessage: string;
  currentPassw: string;
  salt = bcrypt.genSaltSync(1);

  constructor(private personService: PersonService, credentialService: CredentialService,
              routeParams: ActivatedRoute,
              private router: Router) {
    this.credentials = credentialService.getCredentials();
    this.isDataLoaded = false;
    this.onEdit = true;
    this.id = routeParams.snapshot.params['id'];
    this.errorMessage = '';
    this.currentPassw = '';
    this.user =  new Person();
  }

  ngOnInit() {
    this.personService.findPerson(this.id).subscribe(data => {
        this.user = data;
        this.isDataLoaded = true;
      },
      () => {
        this.errorMessage = 'Could not find user!';
      });
  }

  onDelete(user: Person) {
    this.personService.delete(user.id).subscribe(() => {
        if (user.id == this.credentials.id) {
          localStorage.clear();
          this.router.navigate(['/api/login']);
        } else {
          this.router.navigate(['/api/persons']);
        }
      },
      () => {
        this.errorMessage = 'Could not delete user!';
      });
  }

  onUpdate() {
    this.personService.findPerson(this.id).subscribe(data => {
        bcrypt.compare(this.currentPassw, data.password, (err, res) => {
          console.log('Compared result', res); 
          if (res == true){
            this.errorMessage = '';
            this.personService.update(this.user).subscribe();
          } else {
            this.errorMessage = 'You entered a wrong password!';
            this.ngOnInit();
          }
        })
        this.currentPassw = '';
      },
      () => {
        this.errorMessage = 'Could not update user!';
      });
  }

  onEditClick() {
    this.onEdit = !this.onEdit;
  }

}
