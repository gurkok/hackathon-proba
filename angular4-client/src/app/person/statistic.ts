export class Statistic {

    averageAge: number;
    averagePrice: number;
    date: Date;

    constructor() {
        this.averageAge = 0;
        this.averagePrice = 0;
        this.date = new Date();
    }

}
