import { Pipe, PipeTransform } from '@angular/core';
import { Person } from "./person";

@Pipe({
  name: 'personFilter'
})
export class PersonPipe implements PipeTransform {

  transform(value: Person[], input: string): any {
    if (input) {
      return value.filter(val => val.username.indexOf(input) >= 0);
    } else {
      return value;
    }
  }

}
