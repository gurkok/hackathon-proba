import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { Router } from '@angular/router';
import { PersonService } from '../person.service';

@Component({
  selector: 'app-person-registration',
  templateUrl: './person-registration.component.html',
  styleUrls: ['./person-registration.component.css']
})
export class PersonRegistrationComponent implements OnInit {

  user: Person;
  errorMsg: string;
  url: any;

  constructor(
    private router: Router,
    private personService: PersonService) {
    this.user = new Person();
    this.errorMsg = '';
  }

  onRegistration() {
    this.user.stars = 5;
    this.personService.save(this.user).subscribe(() => this.gotoLoginPage(),
      (err) => {
        console.log(err);
        this.errorMsg = 'Could not registrate!'
      });
  }

  gotoLoginPage() {
    this.router.navigate(['/api/login']);
  }

  ngOnInit(): void {
    ;
  }

}
