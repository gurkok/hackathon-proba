import { Injectable } from '@angular/core';
import { Person } from "./person"
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Statistic } from './statistic';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private url: string;
  private statisticUrl: string;

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080/api/persons';
    this.statisticUrl = 'http://localhost:8080/api/statistics';
  }

  public findAll(): Observable<Person[]> {
    return this.http.get<Person[]>(this.url, { withCredentials: true });
  }

  public findPerson(id: number): Observable<Person> {
    console.log(this.url);
    return this.http.get<Person>(this.url + "/" + id, { withCredentials: true });
  }

  public delete(id: number) {
    return this.http.delete(this.url + '/' + id, { withCredentials: true });
  }

  public save(person: Person) {
    return this.http.post<Person>(this.url, person, { withCredentials: true });
  }

  public update(person: Person) {
    return this.http.put<Person>(this.url + "/" + person.id, person, { withCredentials: true });
  }

  public login(user: Person) {
    const loginForm = new FormData();
    loginForm.append('username', user.username);
    loginForm.append('password', user.password);
    loginForm.append('submit', 'login');
    return this.http.post<any>('http://localhost:8080/login', loginForm, { withCredentials: true });
  }

  public logout() {
    return this.http.post('http://localhost:8080/logout', {}, { withCredentials: true });
  }

  public findAllStatistics(): Observable<Statistic[]> {
    return this.http.get<Statistic[]>(this.statisticUrl, { withCredentials: true });
  }

}
