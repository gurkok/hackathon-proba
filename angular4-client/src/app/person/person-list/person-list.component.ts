import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonService } from '../person.service';
import { Person } from '../person';
import { Statistic } from '../statistic';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {

  persons: Person[];
  errorMsg: string;
  page: number;
  username: string;
  statistics: Statistic[];

  constructor(private router: Router, private personService: PersonService) {
    this.persons = [];
    this.errorMsg = '';
    this.page = 0;
    this.username = '';
    this.statistics = [];
  }

  ngOnInit(): void {
    this.onRightArrow();
  }

  public onRightArrow() {
    this.page = this.page + 1;
    this.getAllUsers();
    this.getAllStatistics();
  }

  public onLeftArrow() {
    this.page = this.page - 1;
    this.getAllUsers();
  }

  public getAllUsers() {
    const observer = {
      next: res => {
        this.persons = res;
      },
      error: (err) => {
        this.errorMsg = 'Could not find requests!';
        console.log(err);
      },
      complete: () => { },
    };
    this.personService.findAll().subscribe(observer);
  }

  public getAllStatistics() {
    const observer = {
      next: res => {
        this.statistics = res;
      },
      error: (err) => {
        this.errorMsg = 'Could not find statistics!';
        console.log(err);
      },
      complete: () => { },
    };
    this.personService.findAllStatistics().subscribe(observer);
  }

  public onDelete(person: Person) {
    const observer = {
      next: () => {
        this.persons = this.persons.filter(deletedPerson => deletedPerson.id !== person.id);
      },
      error: () => this.errorMsg = 'Could not delete request!',
      complete: () => { },
    };
    this.personService.delete(person.id).subscribe(observer);
  }

}
