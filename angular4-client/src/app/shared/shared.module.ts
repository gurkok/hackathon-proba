import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../menu/menu/menu.component';

@NgModule({
 imports:      [ RouterModule, CommonModule ],
 declarations: [ MenuComponent ],
 exports:      [ MenuComponent, CommonModule, FormsModule ]
})
export class SharedModule { }