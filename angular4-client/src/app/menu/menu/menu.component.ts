import { Component, OnInit } from '@angular/core';
import { Credentials } from 'src/app/credential/credentials';
import { PersonService } from 'src/app/person/person.service';
import { Router } from '@angular/router';
import { CredentialService } from 'src/app/credential/credential.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  credentials: Credentials;
  clicked = false;

  constructor(private router: Router, private personService: PersonService, private credentialService: CredentialService) {
    this.credentials = credentialService.getCredentials();
  }

  public logout() {
    this.personService.logout().subscribe(() => {
      localStorage.clear();
      this.router.navigate(['api/login']);
    }
    );
  }

  public profile(id: number): void {
    this.router.navigate(['api/persons/' + id]);
  }

  ngOnInit(): void {
    ;
  }

}
