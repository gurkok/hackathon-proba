<%@ page import="edu.bbte.idde.gaim1915.backend.model.CarAd" %>
<%@ page import="java.util.Collection" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="global.css">
    <title>CarAds</title>
</head>

<body>
<h1>
    My car ads
    <form action="${pageContext.request.contextPath}/logout">
        <input type="submit" value="Logout">
    </form>
</h1>
<% for (CarAd carAd : (Collection<CarAd>) request.getAttribute("carAds")) { %>
<div>
    <h2><%=carAd.getCarName()%>
    </h2>
    <label>Horsepower: </label><%=carAd.getHorsePower()%> <br>
    <label>Price: </label><%=carAd.getPrice()%> <%=carAd.getCurrency()%> <br>
    <label>Owner: <%=carAd.getOwner()%> <br>
        <hr>
</div>
<% } %>
</body>

</html>

