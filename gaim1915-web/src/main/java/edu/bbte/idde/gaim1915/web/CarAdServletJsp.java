package edu.bbte.idde.gaim1915.web;

import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.memory.CarAdInMemoryRepository;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/carAdsJsp")
public class CarAdServletJsp extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(CarAdServletJsp.class);
    private transient CarAdRepository carAdRepo = new CarAdInMemoryRepository();

    @Override
    public void init() {
        carAdRepo = new CarAdInMemoryRepository();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Get request arrived to jsp servlet");
        req.setAttribute("carAds", carAdRepo.findAll());

        req.getRequestDispatcher("carAds.jsp").forward(req, resp);
    }

}
