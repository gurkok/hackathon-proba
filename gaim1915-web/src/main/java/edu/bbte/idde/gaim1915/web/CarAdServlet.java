package edu.bbte.idde.gaim1915.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import edu.bbte.idde.gaim1915.backend.model.CarAd;
import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.DaoFactory;
import edu.bbte.idde.gaim1915.backend.repository.PersonRepository;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/carAds")
public class CarAdServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(CarAdServlet.class);
    private transient CarAdRepository carAdRepo;
    private transient PersonRepository personRepo;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void init() {
        carAdRepo = DaoFactory.getInstance().getCarAdRepository();
        personRepo = DaoFactory.getInstance().getPersonRepository();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Get request arrived to servlet");
        String carID = req.getParameter("carID");
        String carName = req.getParameter("carName");

        if (carID == null && carName == null) {
            objectMapper.writeValue(resp.getOutputStream(), carAdRepo.findAll());
            LOG.info("All of the carIDs can be seen");
        } else if (carID != null && carName == null) {
            try {
                CarAd carAd = carAdRepo.findByID(Long.valueOf(carID));
                if (carAd == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    resp.getOutputStream().print("The car with " + carID + "doesn't exist");
                    LOG.info("The car with id {} doesn't exist", carID);
                } else {
                    objectMapper.writeValue(resp.getOutputStream(), carAd);
                    LOG.info("The searched carAd with id {} is {}", carID, carAd.getCarName());
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getOutputStream().print("Invalid input format " + carID);
                LOG.error("Invalid input format {}", carID);
            }
        } else if (carID == null) {
            objectMapper.writeValue(resp.getOutputStream(), carAdRepo.findByCarName(carName));
            LOG.info("Selected cars with  name {}", carName);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getOutputStream().print("Invalid request! Can't contain id and name!");
            LOG.error("Invalid request! Can't contain id and name!");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Post request arrived to servlet");
        try {
            CarAd carAd = objectMapper.readValue(req.getInputStream(), CarAd.class);
            LOG.info("{} carAd was received}", carAd);
            if (carAdIsValid(carAd)) {
                CarAd newCarAd = carAdRepo.create(carAd);
                if (newCarAd == null) {
                    resp.getOutputStream().print("The person's id " + carAd.getPersonID() + "does not exist!");
                    LOG.error("The person's id {} does not exist!", carAd.getPersonID());
                } else {
                    LOG.info("Successfully added carAD {}", newCarAd);
                    resp.getOutputStream().print("Succesfully added car " + newCarAd.getCarName());
                }
            } else {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getOutputStream().print("Invalid input! Contains null values!");
                LOG.error("Invalid input {}", carAd);
            }
        } catch (MismatchedInputException e) {
            resp.getOutputStream().print("Mismatched input!");
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            LOG.error("Mismatched input at post!");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Put request arrived to servlet");
        String carID = req.getParameter("carID");

        if (carID == null) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getOutputStream().print("Bad request! CarID is missing!");
            LOG.error("Bad request! CarID is missing!");
        } else {
            try {
                CarAd carAd = objectMapper.readValue(req.getInputStream(), CarAd.class);
                LOG.info("{} carAd was received}", carAd);

                if (carAdIsValid(carAd)) {
                    CarAd updatedCar = carAdRepo.update(Long.valueOf(carID), carAd);
                    if (updatedCar == null) {
                        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        resp.getOutputStream().print("The car's id " + carID
                                + " or the person's id does not exist! Could not update!");
                        LOG.error("The car'sid {} or the person's id does not exist! " + "Could not update!", carID);
                    } else {
                        resp.getOutputStream().print("Succesfully updated car " + carAd.getCarName()
                                + " with id " + carID);
                        LOG.info("Successfully update carAD {}", carAd);
                    }
                } else {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getOutputStream().print("Invalid input! Contains null values!");
                    LOG.error("Invalid input {} at put", carAd);
                }
            } catch (MismatchedInputException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getOutputStream().print("Mismatched input at put!");
                LOG.error("Mismatched input at put!");
            }
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Delete request arrived to servlet");

        String carID = req.getParameter("carID");
        if (carID == null) {
            resp.getOutputStream().print("Did not receive id! Can not delete!");
            LOG.error("Could not delete without id");
        } else {
            try {
                CarAd deletedCar = carAdRepo.delete(Long.valueOf(carID));
                if (deletedCar == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    resp.getOutputStream().print("The car with " + carID + "doesn't exist");
                    LOG.info("The car with id {} doesn't exist", carID);
                } else {
                    objectMapper.writeValue(resp.getOutputStream(), deletedCar + "was deleted");
                    LOG.info("The deleted carAd with id {} is {}", carID, deletedCar.getCarName());
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getOutputStream().print("Invalid input format " + carID);
                LOG.error("Invalid input format {}", carID);
            }
        }
    }

    private boolean carAdIsValid(CarAd carAd) {
        return carAd != null && carAd.getPersonID() != null && carAd.getCarName() != null
                && carAd.getCurrency() != null && carAd.getPrice() != null
                && carAd.getHorsePower() != null && personRepo.exists(carAd.getPersonID());
    }

}
