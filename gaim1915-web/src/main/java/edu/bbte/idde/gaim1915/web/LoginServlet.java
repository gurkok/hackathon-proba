package edu.bbte.idde.gaim1915.web;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(LoginServlet.class);

    private static final String USERNAME = "Ana";
    private static final String PASSWORD = "Ana";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        LOG.info("Post request arrived to servlet");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (USERNAME.equals(username) && PASSWORD.equals(password)) {
            req.getSession().setAttribute("loggedIn", "true");
            resp.sendRedirect("carAdsJsp");
        } else {
            LOG.info("Failed to login with credentials {} {}", username, password);
            req.setAttribute("success", "Could not login! Incorrect values!");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }
    }

}
