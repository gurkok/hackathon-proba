package edu.bbte.idde.gaim1915.web;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebFilter("/carAdsJsp/*")
public class LoggedOutFilter extends HttpFilter {

    private static final Logger LOG = LoggerFactory.getLogger(LoggedOutFilter.class);

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        LOG.info("Filters the not logged in users");
        if (isLoggedIn(req)) {
            res.sendRedirect("login");
        } else {
            chain.doFilter(req, res);
        }
    }

    private boolean isLoggedIn(HttpServletRequest req) {
        return req.getSession() == null || req.getSession().getAttribute("loggedIn") == null
                || !"true".equals(req.getSession().getAttribute("loggedIn"));
    }

}
