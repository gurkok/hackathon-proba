package edu.bbte.idde.gaim1915.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import edu.bbte.idde.gaim1915.backend.model.Person;
import edu.bbte.idde.gaim1915.backend.repository.DaoFactory;
import edu.bbte.idde.gaim1915.backend.repository.PersonRepository;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/persons")
public class PersonServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(PersonServlet.class);
    private transient PersonRepository personRepo;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void init() {
        personRepo = DaoFactory.getInstance().getPersonRepository();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Get request arrived to servlet");
        String personID = req.getParameter("personID");
        String name = req.getParameter("name");

        if (personID == null && name == null) {
            objectMapper.writeValue(resp.getOutputStream(), personRepo.findAll());
            LOG.info("All of the personIDs can be seen");
        } else if (personID != null && name == null) {
            try {
                Person person = personRepo.findByID(Long.valueOf(personID));
                if (person == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    resp.getOutputStream().print("The person with " + personID + "doesn't exist");
                    LOG.info("The person with id {} doesn't exist", personID);
                } else {
                    objectMapper.writeValue(resp.getOutputStream(), person);
                    LOG.info("The searched person with id {} is {}", personID, person.getName());
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getOutputStream().print("Invalid input format " + personID);
                LOG.error("Invalid input format {}", personID);
            }
        } else if (personID == null) {
            objectMapper.writeValue(resp.getOutputStream(), personRepo.findByName(name));
            LOG.info("Selected persons with  name {}", name);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getOutputStream().print("Invalid request! Can't contain id and name!");
            LOG.error("Invalid request! Can't contain id and name!");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Post request arrived to servlet");
        try {
            Person person = objectMapper.readValue(req.getInputStream(), Person.class);
            LOG.info("{} person was received}", person);
            if (personIsValid(person)) {
                Person newPerson = personRepo.create(person);
                if (newPerson == null) {
                    resp.getOutputStream().print("The person's id " + person.getId() + "does not exist !");
                    LOG.error("The person's id " + person.getId() + "does not exist !");
                } else {
                    LOG.info("Successfully added person {}", newPerson);
                    resp.getOutputStream().print("Succesfully added person " + newPerson.getName());
                }
            } else {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getOutputStream().print("Invalid input! Contains null values!");
                LOG.error("Invalid input {}", person);
            }
        } catch (MismatchedInputException e) {
            resp.getOutputStream().print("Mismatched input!");
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            LOG.error("Mismatched input at post!");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Put request arrived to servlet");
        String personID = req.getParameter("personID");

        if (personID == null) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getOutputStream().print("Bad request! personID is missing!");
            LOG.error("Bad request! personID is missing!");
        } else {
            try {
                Person person = objectMapper.readValue(req.getInputStream(), Person.class);
                LOG.info("{} person was received}", person);

                if (personIsValid(person)) {
                    Person updatedperson = personRepo.update(Long.valueOf(personID), person);
                    if (updatedperson == null) {
                        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        resp.getOutputStream().print("The person's id " + personID
                                + "does not exist! Could not update!");
                        LOG.error("The person's id {} does not exist! " + "Could not update!", personID);
                    } else {
                        resp.getOutputStream().print("Succesfully updated person " + person.getName()
                                + " with id " + personID);
                        LOG.info("Successfully update person {}", person);
                    }
                } else {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getOutputStream().print("Invalid input! Contains null values!");
                    LOG.error("Invalid input {} at put", person);
                }
            } catch (MismatchedInputException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getOutputStream().print("Mismatched input at put!");
                LOG.error("Mismatched input at put!");
            }
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Delete request arrived to servlet");

        String personID = req.getParameter("personID");
        if (personID == null) {
            resp.getOutputStream().print("Did not receive id! Can not delete!");
            LOG.error("Could not delete without id");
        } else {
            try {
                Person deletedperson = personRepo.delete(Long.valueOf(personID));
                if (deletedperson == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    resp.getOutputStream().print("The person with " + personID + "doesn't exist");
                    LOG.info("The person with id {} doesn't exist", personID);
                } else {
                    objectMapper.writeValue(resp.getOutputStream(), deletedperson + "was deleted");
                    LOG.info("The deleted person with id {} is {}", personID, deletedperson.getName());
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getOutputStream().print("Invalid input format " + personID);
                LOG.error("Invalid input format {}", personID);
            }
        }
    }

    private boolean personIsValid(Person person) {
        return person != null && person.getName() != null && person.getAge() != null
                && person.getStars() != null && person.getPhoneNr() != null;
    }

}
