package edu.bbte.idde.gaim1915.backend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class ConfigFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigFactory.class);

    private static final String FILE = getFile();

    private static PropertyConfig propertyConfig = new PropertyConfig();

    static {
        ObjectMapper jsonMapper = new ObjectMapper();
        try (InputStream inputStream = ConfigFactory.class.getResourceAsStream(FILE)) {
            propertyConfig = jsonMapper.readValue(inputStream, PropertyConfig.class);
            LOG.info("Read configs from json file are {}", propertyConfig);
        } catch (IOException e) {
            LOG.error("ERROR while reading the config from the json file!!");
        }
    }

    public static PropertyConfig.Database getDatabase() {
        return propertyConfig.getDatabase();
    }

    public static String getProfile() {
        return propertyConfig.getProfile();
    }

    public static String getFile() {
        String profile = System.getenv("PROFILE");
        LOG.info("Profile is: {}", profile);
        return "/application-" + profile + ".json";
    }
}
