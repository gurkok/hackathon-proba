package edu.bbte.idde.gaim1915.backend.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public abstract class BaseEntity {

    protected Long id;

}
