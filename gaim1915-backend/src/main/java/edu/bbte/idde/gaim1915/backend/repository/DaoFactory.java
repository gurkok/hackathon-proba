package edu.bbte.idde.gaim1915.backend.repository;

import edu.bbte.idde.gaim1915.backend.config.ConfigFactory;
import edu.bbte.idde.gaim1915.backend.repository.jdbc.JdbcDaoFactory;
import edu.bbte.idde.gaim1915.backend.repository.memory.MemoryDaoFactory;

public abstract class DaoFactory {

    private static final String MEMORY_PROFILE = "Memory";
    private static final String DATABASE_PROFILE = "Database";

    private static volatile DaoFactory daoFactoryInst;

    public abstract PersonRepository getPersonRepository();

    public abstract CarAdRepository getCarAdRepository();

    public static synchronized DaoFactory getInstance() {
        if (daoFactoryInst == null) {
            String profile = ConfigFactory.getProfile();
            if (MEMORY_PROFILE.equals(profile)) {
                daoFactoryInst = new MemoryDaoFactory();
            } else if (DATABASE_PROFILE.equals(profile)) {
                daoFactoryInst = new JdbcDaoFactory();
            }
        }
        return daoFactoryInst;
    }

}
