package edu.bbte.idde.gaim1915.backend.repository;

import edu.bbte.idde.gaim1915.backend.model.Person;

import java.util.Collection;

public interface PersonRepository extends Repository<Person> {

    Collection<Person> findByName(String name);

}
