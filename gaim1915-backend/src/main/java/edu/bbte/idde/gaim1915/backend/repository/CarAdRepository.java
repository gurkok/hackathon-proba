package edu.bbte.idde.gaim1915.backend.repository;

import edu.bbte.idde.gaim1915.backend.model.CarAd;

import java.util.Collection;

public interface CarAdRepository extends Repository<CarAd> {

    Collection<CarAd> findByCarName(String carName);

    void deleteByPersonId(Long personID);

}
