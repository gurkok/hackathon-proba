package edu.bbte.idde.gaim1915.backend.repository.jdbc;

import edu.bbte.idde.gaim1915.backend.model.Person;
import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.DaoFactory;
import edu.bbte.idde.gaim1915.backend.repository.PersonRepository;
import edu.bbte.idde.gaim1915.backend.repository.RepoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class PersonJdbcRepository implements PersonRepository {

    private static final Logger LOG = LoggerFactory.getLogger(PersonJdbcRepository.class);

    ConnectionFactory connectionFactory;

    public PersonJdbcRepository() {
        connectionFactory = ConnectionFactory.getConnectionFactoryInstance();
    }

    @Override
    public Person create(Person entity) {
        if (entity.getId() == null) {
            Connection connection = connectionFactory.pullConnection();
            try {
                String query = "INSERT INTO Person VALUES (default, ?, ?, ?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                setPreparedStatementByPerson(entity, preparedStatement);
                int row = preparedStatement.executeUpdate();
                if (row == 0) {
                    LOG.warn("Did not insert anything");
                }
                return entity;
            } catch (SQLException e) {
                LOG.warn("Sql exception could not create Person with id {}", entity.getId());
                throw new RepoException("Sql exception could not create Person", e);
            } finally {
                connectionFactory.putConnection(connection);
            }
        } else {
            LOG.warn("The person with id {} exists!", entity.getId());
            return null;
        }
    }

    @Override
    public Person delete(Long id) {
        CarAdRepository carAdJdbcRepository = DaoFactory.getInstance().getCarAdRepository();
        Person person = findByID(id);
        if (person == null) {
            LOG.warn("The person with id {} does not exist!", id);
            return null;
        } else {
            Connection connection = connectionFactory.pullConnection();
            try {
                carAdJdbcRepository.deleteByPersonId(id);
                String query = "DELETE FROM Person WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(1, id);
                int row = preparedStatement.executeUpdate();
                if (row > 0) {
                    LOG.info("Successfully deleted person with id {}", id);
                    return person;
                } else {
                    LOG.warn("Did not delete person with id {}", id);
                    return null;
                }
            } catch (SQLException e) {
                LOG.warn("Could not delete person with id {}", id);
                throw new RepoException("Sql exception could not delete person", e);
            } finally {
                connectionFactory.putConnection(connection);
            }
        }
    }

    @Override
    public Person findByID(Long id) {
        if (id == null) {
            LOG.warn("Id id null! Could not find!");
            return null;
        }
        Connection connection = connectionFactory.pullConnection();
        try {
            Person person = null;
            String query = "SELECT * FROM Person WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultset = preparedStatement.executeQuery();
            if (resultset.next()) {
                person = getPersonByResultSet(resultset);
                LOG.info("Found person with id {}", id);
            } else {
                LOG.info("Did not find person with id {}", id);
            }
            return person;
        } catch (SQLException e) {
            LOG.warn("Sql exception could not find Person with id {}", id);
            throw new RepoException("Sql exception could not put person", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public Person update(Long id, Person entity) {
        Connection connection = connectionFactory.pullConnection();
        try {
            if (exists(id)) {
                String query = "UPDATE Person SET name = ?, age = ?, phoneNr = ?, stars = ? WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(5, id);
                setPreparedStatementByPerson(entity, preparedStatement);
                int row = preparedStatement.executeUpdate();
                if (row == 0) {
                    LOG.warn("Did not update anything for id {}", id);
                }
                return findByID(id);
            } else {
                LOG.warn("Person with id {} does not exist!", id);
                return null;
            }
        } catch (SQLException e) {
            LOG.warn("SqlException Could not update person with id {}", id);
            throw new RepoException("Sql exception could not update person", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public Collection<Person> findAll() {
        Connection connection = connectionFactory.pullConnection();
        try {
            Collection<Person> persons = new ArrayList<>();
            String query = "SELECT * FROM Person";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultset = preparedStatement.executeQuery();
            while (resultset.next()) {
                Person person = getPersonByResultSet(resultset);
                persons.add(person);
            }
            return persons;
        } catch (SQLException e) {
            LOG.warn("Sql exception could not find persons");
            throw new RepoException("Sql exception could not find persons", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public boolean exists(Long id) {
        Person person = findByID(id);
        return person != null;
    }

    @Override
    public Collection<Person> findByName(String name) {
        if (name == null) {
            LOG.warn("Name id null! Could not find!");
            return null;
        }
        Connection connection = connectionFactory.pullConnection();
        try {
            Collection<Person> persons = new ArrayList<>();
            String query = "SELECT * FROM Person WHERE name = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, name);
            ResultSet resultset = preparedStatement.executeQuery();
            while (resultset.next()) {
                Person person = getPersonByResultSet(resultset);
                persons.add(person);
            }
            return persons;
        } catch (SQLException e) {
            LOG.warn("Sql exception could not find persons");
            throw new RepoException("Sql exception could not find persons", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    private Person getPersonByResultSet(ResultSet resultset) throws SQLException {
        Person person = new Person();
        person.setId(resultset.getLong(1));
        person.setName(resultset.getString(2));
        person.setAge(resultset.getInt(3));
        person.setPhoneNr(resultset.getString(4));
        person.setStars(resultset.getDouble(5));
        return person;
    }

    private void setPreparedStatementByPerson(Person entity, PreparedStatement preparedStatement)
            throws SQLException {
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getAge());
        preparedStatement.setString(3, entity.getPhoneNr());
        preparedStatement.setDouble(4, entity.getStars());
    }

}
