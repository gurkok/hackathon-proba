package edu.bbte.idde.gaim1915.backend.repository.memory;

import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.DaoFactory;
import edu.bbte.idde.gaim1915.backend.repository.PersonRepository;

public class MemoryDaoFactory extends DaoFactory {

    private PersonRepository personRepository;
    private CarAdRepository carAdRepository;

    @Override
    public PersonRepository getPersonRepository() {
        if (personRepository == null) {
            personRepository = new PersonInMemoryRepository();
        }
        return personRepository;
    }

    @Override
    public CarAdRepository getCarAdRepository() {
        if (carAdRepository == null) {
            carAdRepository = new CarAdInMemoryRepository();
        }
        return carAdRepository;
    }

}
