package edu.bbte.idde.gaim1915.backend.repository.memory;

import edu.bbte.idde.gaim1915.backend.model.Person;
import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.DaoFactory;
import edu.bbte.idde.gaim1915.backend.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class PersonInMemoryRepository implements PersonRepository {

    private static final Logger LOG = LoggerFactory.getLogger(PersonInMemoryRepository.class);

    private final ConcurrentHashMap<Long, Person> personDatabase = new ConcurrentHashMap<>();
    private final AtomicLong personId = new AtomicLong(0);

    public PersonInMemoryRepository() {
        this.create(new Person("Anna", 19, "123", 5.6));
        this.create(new Person("Radu", 25, "123", 9.2));
        this.create(new Person("Emil", 10, "122343", 15.5));
        this.create(new Person("Peter", 11, "123523", 35.3));
    }

    @Override
    public Person create(Person entity) {
        entity.setId(personId.getAndIncrement());
        personDatabase.put(entity.getId(), entity);

        return personDatabase.get(entity.getId());
    }

    @Override
    public Person delete(Long id) {
        CarAdRepository carAdRepository = DaoFactory.getInstance().getCarAdRepository();
        if (exists(id)) {
            carAdRepository.deleteByPersonId(id);
            Person person = findByID(id);
            personDatabase.remove(id);
            return person;
        } else {
            LOG.warn("Person with id {} does not exist! Could not delete!", id);
            return null;
        }
    }

    @Override
    public Person findByID(Long id) {
        if (!exists(id)) {
            LOG.warn("Person with id {} does not exist!", id);
            return null;
        }
        return personDatabase.get(id);
    }

    @Override
    public Person update(Long id, Person entity) {
        return null;
    }

    @Override
    public Collection<Person> findAll() {
        return personDatabase.values();
    }

    @Override
    public boolean exists(Long id) {
        return personDatabase.containsKey(id);
    }

    @Override
    public Collection<Person> findByName(String name) {
        Collection<Person> persons = new ArrayList<>();
        for (Person person : personDatabase.values()) {
            if (person.getName().equals(name)) {
                persons.add(person);
            }
        }
        return persons;
    }
}
