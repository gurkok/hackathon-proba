package edu.bbte.idde.gaim1915.backend.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Person extends BaseEntity {

    private String name;
    private Integer age;
    private String phoneNr;
    private Double stars;

}
