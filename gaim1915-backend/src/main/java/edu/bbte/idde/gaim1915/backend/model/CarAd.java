package edu.bbte.idde.gaim1915.backend.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class CarAd extends BaseEntity {
    private String carName;
    private Integer horsePower;
    private Double price;
    private String currency;
    private Long personID;
}
