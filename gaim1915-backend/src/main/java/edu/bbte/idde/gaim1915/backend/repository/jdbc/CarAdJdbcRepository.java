package edu.bbte.idde.gaim1915.backend.repository.jdbc;

import edu.bbte.idde.gaim1915.backend.model.CarAd;
import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.RepoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class CarAdJdbcRepository implements CarAdRepository {
    private static final Logger LOG = LoggerFactory.getLogger(CarAdJdbcRepository.class);

    ConnectionFactory connectionFactory;

    public CarAdJdbcRepository() {
        connectionFactory = ConnectionFactory.getConnectionFactoryInstance();
    }

    @Override
    public CarAd create(CarAd entity) {
        Connection connection = connectionFactory.pullConnection();
        try {
            String query = "INSERT INTO CarAd VALUES (default, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            setPreparedStatementByCarAd(entity, preparedStatement);
            int row = preparedStatement.executeUpdate();
            if (row == 0) {
                LOG.warn("Did not insert anything");
            }
            return entity;
        } catch (SQLException e) {
            LOG.warn("Sql exception could not create CarAd");
            throw new RepoException("Sql exception could not create CarAd", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public CarAd delete(Long id) {
        Connection connection = connectionFactory.pullConnection();
        try {
            CarAd carAd = findByID(id);
            if (carAd == null) {
                LOG.warn("The carAd with id {} does not exist!", id);
                return null;
            } else {
                String query = "DELETE FROM CarAd WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(1, id);
                int row = preparedStatement.executeUpdate();
                if (row > 0) {
                    LOG.info("Successfully deleted car with id {}", id);
                    return carAd;
                } else {
                    LOG.warn("Did not delete car with id {}", id);
                    return null;
                }
            }
        } catch (SQLException e) {
            LOG.warn("Could not delete car with id {}", id);
            throw new RepoException("Could not delete car", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public void deleteByPersonId(Long personId) {
        Connection connection = connectionFactory.pullConnection();
        try {
            String query = "DELETE FROM CarAd WHERE personID = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, personId);
            int row = preparedStatement.executeUpdate();
            if (row > 0) {
                LOG.info("Successfully deleted car with personID {}", personId);
            } else {
                LOG.warn("Did not delete any cars with personID {}", personId);
            }
        } catch (SQLException e) {
            LOG.warn("Could not delete car with personID {}", personId);
            throw new RepoException("Could not delete car", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public CarAd findByID(Long id) {
        if (id == null) {
            LOG.warn("Id id null! Could not find!");
            return null;
        }
        Connection connection = connectionFactory.pullConnection();
        try {
            CarAd carAd = null;
            String query = "SELECT * FROM CarAd WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultset = preparedStatement.executeQuery();
            if (resultset.next()) {
                carAd = getCarAdByResultSet(resultset);
                LOG.info("Found carAd with id {}", id);
            } else {
                LOG.info("Did not find carAd with id {}", id);
            }
            return carAd;
        } catch (SQLException e) {
            LOG.warn("Sql exception could not find carAd with id {}", id);
            throw new RepoException("Sql exception could not find CarAd", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public CarAd update(Long id, CarAd entity) {
        Connection connection = connectionFactory.pullConnection();
        try {
            if (exists(id)) {
                String query = "UPDATE CarAd SET carName = ?, horsePower = ?, price = ?, "
                        + "currency = ?, personID = ? WHERE id = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                setPreparedStatementByCarAd(entity, preparedStatement);
                preparedStatement.setLong(6, id);
                int row = preparedStatement.executeUpdate();
                if (row == 0) {
                    LOG.warn("Did not update anything for id {}", id);
                }
                return findByID(id);
            } else {
                LOG.warn("Car with id {} does not exist!", id);
                return null;
            }
        } catch (SQLException e) {
            LOG.warn("SqlException Could not update carAd with id {}", id);
            throw new RepoException("Sql exception could not update CarAd", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public Collection<CarAd> findAll() {
        Connection connection = connectionFactory.pullConnection();
        try {
            Collection<CarAd> carAds = new ArrayList<>();
            String query = "SELECT * FROM CarAd";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultset = preparedStatement.executeQuery();
            while (resultset.next()) {
                CarAd carAd = getCarAdByResultSet(resultset);
                carAds.add(carAd);
            }
            return carAds;
        } catch (SQLException e) {
            LOG.warn("Sql exception could not find carAds");
            throw new RepoException("Sql exception could not find CarAds", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    @Override
    public boolean exists(Long id) {
        CarAd carAd = findByID(id);
        return carAd != null;
    }

    @Override
    public Collection<CarAd> findByCarName(String carName) {
        if (carName == null) {
            LOG.warn("CarName is null! Could not find!");
            return null;
        }
        Connection connection = connectionFactory.pullConnection();
        Collection<CarAd> carAds = new ArrayList<>();
        try {
            String query = "SELECT * FROM CarAd WHERE carName = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, carName);
            ResultSet resultset = preparedStatement.executeQuery();
            while (resultset.next()) {
                CarAd carAd = getCarAdByResultSet(resultset);
                carAds.add(carAd);
            }
            return carAds;
        } catch (SQLException e) {
            LOG.warn("Sql exception could not find carAds with carName {}", carName);
            throw new RepoException("Sql exception could not find CarAds", e);
        } finally {
            connectionFactory.putConnection(connection);
        }
    }

    private CarAd getCarAdByResultSet(ResultSet resultset) throws SQLException {
        CarAd carAd = new CarAd();
        carAd.setId(resultset.getLong(1));
        carAd.setCarName(resultset.getString(2));
        carAd.setHorsePower(resultset.getInt(3));
        carAd.setPrice(resultset.getDouble(4));
        carAd.setCurrency(resultset.getString(5));
        carAd.setPersonID(resultset.getLong(6));
        return carAd;
    }

    private void setPreparedStatementByCarAd(CarAd entity, PreparedStatement preparedStatement)
            throws SQLException {
        preparedStatement.setString(1, entity.getCarName());
        preparedStatement.setInt(2, entity.getHorsePower());
        preparedStatement.setDouble(3, entity.getPrice());
        preparedStatement.setString(4, entity.getCurrency());
        preparedStatement.setLong(5, entity.getPersonID());
    }
}
