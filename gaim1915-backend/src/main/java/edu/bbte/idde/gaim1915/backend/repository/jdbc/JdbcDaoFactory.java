package edu.bbte.idde.gaim1915.backend.repository.jdbc;

import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.DaoFactory;
import edu.bbte.idde.gaim1915.backend.repository.PersonRepository;

public class JdbcDaoFactory extends DaoFactory {

    private PersonRepository personRepository;
    private CarAdRepository carAdRepository;

    @Override
    public PersonRepository getPersonRepository() {
        if (personRepository == null) {
            personRepository = new PersonJdbcRepository();
        }
        return personRepository;
    }

    @Override
    public CarAdRepository getCarAdRepository() {
        if (carAdRepository == null) {
            carAdRepository = new CarAdJdbcRepository();
        }
        return carAdRepository;
    }

}
