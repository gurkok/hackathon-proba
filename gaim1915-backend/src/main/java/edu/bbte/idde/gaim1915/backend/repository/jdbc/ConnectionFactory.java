package edu.bbte.idde.gaim1915.backend.repository.jdbc;

import edu.bbte.idde.gaim1915.backend.config.ConfigFactory;
import edu.bbte.idde.gaim1915.backend.config.PropertyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Deque;
import java.util.LinkedList;

class ConnectionFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ConnectionFactory.class);

    private static ConnectionFactory connectionFactoryInst;
    private final Deque<Connection> connections = new LinkedList<>();

    private Integer poolSize;

    public ConnectionFactory() {
        try {
            LOG.info("Setting up the connection with the database");
            PropertyConfig.Database database = ConfigFactory.getDatabase();
            Class.forName(database.getDriver());
            poolSize = database.getPoolSize();
            for (int i = 0; i < poolSize; i++) {
                connections.add(DriverManager.getConnection(database.getSqlUrl(),
                        database.getUsername(), database.getPassword()));
            }
        } catch (SQLException | ClassNotFoundException e) {
            LOG.error("Could not set up the connection!");
        }
    }

    public synchronized Connection pullConnection() {
        if (!connections.isEmpty()) {
            LOG.info("Pulling new connection!");
            return connections.removeLast();
        }
        LOG.warn("No connection in the pool!");
        return null;
    }

    public synchronized void putConnection(Connection connection) {
        if (connections.size() >= poolSize) {
            LOG.warn("The pool is already full!");
        } else {
            connections.add(connection);
            LOG.info("A connection was retrieved!");
        }
    }

    public static synchronized ConnectionFactory getConnectionFactoryInstance() {
        if (connectionFactoryInst == null) {
            connectionFactoryInst = new ConnectionFactory();
        }
        return connectionFactoryInst;
    }

}
