package edu.bbte.idde.gaim1915.backend.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PropertyConfig {

    @JsonProperty
    private Database database;

    @JsonProperty
    private String profile;

    @Data
    @ToString
    public static class Database {
        @JsonProperty
        private String sqlUrl;
        @JsonProperty
        private String driver;
        @JsonProperty
        private String username;
        @JsonProperty
        private String password;
        @JsonProperty
        private Integer poolSize;
    }

}
