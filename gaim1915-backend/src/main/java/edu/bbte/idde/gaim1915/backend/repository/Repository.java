package edu.bbte.idde.gaim1915.backend.repository;

import edu.bbte.idde.gaim1915.backend.model.BaseEntity;

import java.util.Collection;

public interface Repository<T extends BaseEntity> {

    T create(T entity);

    T delete(Long id);

    T findByID(Long id);

    T update(Long id, T entity);

    Collection<T> findAll();

    boolean exists(Long id);

}
