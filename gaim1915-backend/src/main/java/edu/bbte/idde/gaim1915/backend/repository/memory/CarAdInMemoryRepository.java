package edu.bbte.idde.gaim1915.backend.repository.memory;

import edu.bbte.idde.gaim1915.backend.model.CarAd;
import edu.bbte.idde.gaim1915.backend.repository.CarAdRepository;
import edu.bbte.idde.gaim1915.backend.repository.DaoFactory;
import edu.bbte.idde.gaim1915.backend.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class CarAdInMemoryRepository implements CarAdRepository {

    private static final Logger LOG = LoggerFactory.getLogger(CarAdInMemoryRepository.class);

    private final ConcurrentHashMap<Long, CarAd> carAdDatabase = new ConcurrentHashMap<>();
    private final AtomicLong carAdId = new AtomicLong(0);

    public CarAdInMemoryRepository() {
        this.create(new CarAd("Volvo", 500, 20.5, "EUR", 1L));
        this.create(new CarAd("Mercedes", 5000, 2000.5, "GBP", 1L));
        this.create(new CarAd("Volkswagen", 10000, 550.5, "RON", 1L));
        this.create(new CarAd("Ferrari", 200, 50000.5, "HU", 1L));
        this.create(new CarAd("Dacia", 3000, 150.8, "RON", 1L));
    }

    @Override
    public CarAd create(CarAd carAdEntity) {
        PersonRepository personRepository = DaoFactory.getInstance().getPersonRepository();
        if (personRepository.exists(carAdEntity.getPersonID())) {
            carAdEntity.setId(carAdId.getAndIncrement());
            carAdDatabase.put(carAdEntity.getId(), carAdEntity);

            return carAdDatabase.get(carAdEntity.getId());
        } else {
            LOG.warn("The person with id {} does not exist!", carAdEntity.getPersonID());
            return null;
        }
    }

    @Override
    public CarAd findByID(Long carAdID) {
        if (!exists(carAdID)) {
            LOG.warn("Car with id {} does not exist!", carAdID);
            return null;
        }
        return carAdDatabase.get(carAdID);
    }

    @Override
    public Collection<CarAd> findByCarName(String carName) {
        Collection<CarAd> carAds = new ArrayList<>();
        for (CarAd carAd : carAdDatabase.values()) {
            if (carAd.getCarName().equals(carName)) {
                carAds.add(carAd);
            }
        }
        return carAds;
    }

    @Override
    public void deleteByPersonId(Long personID) {
        for (CarAd carAd : carAdDatabase.values()) {
            if (carAd.getPersonID().equals(personID)) {
                carAdDatabase.remove(carAd.getId());
            }
        }
    }

    @Override
    public Collection<CarAd> findAll() {
        return carAdDatabase.values();
    }

    @Override
    public CarAd update(Long carID, CarAd carAd) {
        if (!exists(carID)) {
            LOG.warn("Car with id {} does not exist!", carAd.getId());
            return null;
        }
        PersonRepository personRepository = DaoFactory.getInstance().getPersonRepository();
        if (personRepository.exists(carAd.getPersonID())) {
            carAd.setId(carID);
            carAdDatabase.put(carID, carAd);
            return carAd;
        } else {
            LOG.warn("The person with id {} does not exist!", carAd.getPersonID());
            return null;
        }
    }

    @Override
    public CarAd delete(Long carAdID) {
        if (exists(carAdID)) {
            CarAd deletedCar = findByID(carAdID);
            carAdDatabase.remove(carAdID);
            return deletedCar;
        } else {
            LOG.warn("Car with id {} does not exist! Could not delete!", carAdID);
            return null;
        }
    }

    @Override
    public boolean exists(Long carAdID) {
        return carAdDatabase.containsKey(carAdID);
    }
}
